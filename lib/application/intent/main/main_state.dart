part of 'main_bloc.dart';

@freezed
abstract class MainState with _$MainState {
  const factory MainState.initial() = _Initial;
  const factory MainState.loading() = _Loading;
  const factory MainState.showScreen(List<Intent> intents, bool isEnabledToSave) = _ShowScreen;
  const factory MainState.saveChangesFailure(IntentFailure intentFailure) = _SaveChangesFailure;
  const factory MainState.loadFailure(IntentFailure intentFailure) = _LoadFailure;
  const factory MainState.successfulDeletionIntent() = _SuccessfulDeletionIntent;
  const factory MainState.successfulDeletionExample() = _SuccessfulDeletionExample;
  const factory MainState.intentAdded() = _IntentAdded; // unused
  const factory MainState.showStatus(String status) = _ShowStatus;
  const factory MainState.loadStatusFailure() = _LoadStatusFailure;
  const factory MainState.successfulSavingData() = _SuccessfulSavingData;
}
