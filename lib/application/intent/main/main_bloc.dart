import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:telegram_bot/domain/intent/i_intent_repository.dart';
import 'package:telegram_bot/domain/intent/intent.dart';
import 'package:telegram_bot/domain/intent/intent_failure.dart';
import 'package:telegram_bot/domain/intent/value_objects.dart';

part 'main_event.dart';
part 'main_state.dart';
part 'main_bloc.freezed.dart';

class MainBloc extends Bloc<MainEvent, MainState> {
  final IIntentRepository _intentRepository;

  MainBloc(this._intentRepository) : super(const MainState.initial()) {
    on<MainEvent>((event, emit) async {
      await event.map(
        saveChanges: (_SaveChanges event) => _saveChanges(event, emit),
        intentChanged: (_IntentChanged event) => _intentChange(event, emit),
        exampleChanged: (_ExampleChanged event) => _exampleChange(event, emit),
        addIntent: (_AddIntent event) => _addIntent(event, emit),
        addExample: (_AddExample event) => _addExample(event, emit),
        deleteIntent: (_DeleteIntent event) => _deleteIntent(event, emit),
        deleteExample: (_DeleteExample event) => _deleteExample(event, emit),
        loadIntents: (_LoadIntents event) => _loadIntents(event, emit),
        loadStatus: (_LoadStatus event) => _loadStatus(event, emit),
      );
    });
  }

  Future _loadIntents(_LoadIntents event, Emitter<MainState> emit) async {
    emit(const _Loading());
    final _result = await _intentRepository.loadIntents();
    _result.fold(
      (f) => emit(_LoadFailure(f)),
      (intents) => emit(_ShowScreen(intents, checkForSaving(intents))),
    );
  }

  Future _loadStatus(_LoadStatus event, Emitter<MainState> emit) async {
    await state.maybeMap(
      showScreen: (state) async {
        emit(const _Loading());
        final _result = await _intentRepository.getStatus();
        _result.fold(
          (f) {
            emit(const _LoadStatusFailure());
            emit(_ShowScreen(state.intents, checkForSaving(state.intents)));
          },
          (status) {
            emit(_ShowStatus(status));
            emit(_ShowScreen(state.intents, checkForSaving(state.intents)));
          },
        );
      },
      orElse: () {},
    );
  }

  Future<void> _saveChanges(_SaveChanges event, Emitter<MainState> emit) async {
    await state.maybeMap(
      showScreen: (state) async {
        emit(const _Loading());
        final _result = await _intentRepository.saveChanges(state.intents);
        _result.fold(
          (f) => emit(_SaveChangesFailure(f)),
          (_) {
            emit(const _SuccessfulSavingData());
            emit(_ShowScreen(state.intents, checkForSaving(state.intents)));
          },
        );
      },
      orElse: () {},
    );
  }

  FutureOr<void> _addExample(_AddExample event, Emitter<MainState> emit) async {
    final newIntent = event.intent.copyWith(examples: List.of(event.intent.examples)..add(Example('')));
    state.maybeMap(
      showScreen: (state) {
        final List<Intent> newList = [];
        for (final intent in state.intents) {
          if (intent.intentBody == newIntent.intentBody) {
            newList.add(newIntent);
          } else {
            newList.add(intent);
          }
        }
        emit(_ShowScreen(newList, checkForSaving(newList)));
      },
      orElse: () {},
    );
  }

  FutureOr<void> _addIntent(_AddIntent event, Emitter<MainState> emit) {
    state.maybeMap(
      showScreen: (state) {
        final List<Intent> newList = [];
        newList.addAll(state.intents);
        newList.add(event.intent);
        emit(const _IntentAdded());
        emit(_ShowScreen(newList, false));
      },
      orElse: () {},
    );
  }

  FutureOr<void> _deleteExample(_DeleteExample event, Emitter<MainState> emit) async {
    final Intent newIntent =
        event.intent.copyWith(examples: List.of(event.intent.examples)..removeAt(event.exampleIndex));
    state.maybeMap(
      showScreen: (state) {
        final List<Intent> newList = [];
        for (final intent in state.intents) {
          if (intent.intentBody == newIntent.intentBody) {
            newList.add(newIntent);
          } else {
            newList.add(intent);
          }
        }
        emit(const _SuccessfulDeletionExample());
        emit(_ShowScreen(newList, checkForSaving(newList)));
      },
      orElse: () {},
    );
  }

  FutureOr<void> _deleteIntent(_DeleteIntent event, Emitter<MainState> emit) async {
    state.maybeMap(
      showScreen: (state) {
        final List<Intent> newList = [];
        for (final intent in state.intents) {
          if (intent.intentBody != event.intent.intentBody) {
            newList.add(intent);
          }
        }
        emit(const _SuccessfulDeletionIntent());
        emit(_ShowScreen(newList, checkForSaving(newList)));
      },
      orElse: () {},
    );
  }

  Future<void> _intentChange(_IntentChanged event, Emitter<MainState> emit) async {
    state.maybeMap(
      showScreen: (state) {
        final List<Intent> newList = [];
        for (final intent in state.intents) {
          if (intent.intentBody.getOrCrash() == event.previousName) {
            newList.add(intent.copyWith(intentBody: IntentBody(event.nextName)));
          } else {
            newList.add(intent);
          }
        }
        emit(_ShowScreen(newList, checkForSaving(newList)));
      },
      orElse: () {},
    );
  }

  Future<void> _exampleChange(_ExampleChanged event, Emitter<MainState> emit) async {
    state.maybeMap(
      showScreen: (state) {
        final newExample = Example(event.example);
        print('Example changed: ${event.example}');
        final List<Intent> newList = [];
        for (final intent in state.intents) {
          if (intent.intentBody == event.intent.intentBody) {
            final newIntent = Intent(intentBody: intent.intentBody, examples: intent.examples);
            newIntent.examples[event.index] = newExample;
            newList.add(newIntent);
          } else {
            newList.add(intent);
          }
        }
        emit(_ShowScreen(newList, checkForSaving(newList)));
      },
      orElse: () {},
    );
  }

  bool checkForSaving(List<Intent> intents) {
    for (final intent in intents) {
      if (intent.examples.isEmpty) return false;
      for (final example in intent.examples) {
        if (example.getOrCrash().trim() == '') return false;
      }
    }
    return true;
  }
}
