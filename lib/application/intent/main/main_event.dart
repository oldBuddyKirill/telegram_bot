part of 'main_bloc.dart';

@freezed
abstract class MainEvent with _$MainEvent {
  const factory MainEvent.loadIntents() = _LoadIntents;
  const factory MainEvent.saveChanges() = _SaveChanges;
  const factory MainEvent.intentChanged(String previousName, String nextName) = _IntentChanged;
  const factory MainEvent.exampleChanged({
    required String example,
    required Intent intent,
    required int index,
  }) = _ExampleChanged;
  const factory MainEvent.addIntent(Intent intent) = _AddIntent;
  const factory MainEvent.addExample(Intent intent) = _AddExample;
  const factory MainEvent.deleteIntent(Intent intent) = _DeleteIntent;
  const factory MainEvent.deleteExample({required int exampleIndex, required Intent intent}) = _DeleteExample;
  const factory MainEvent.loadStatus() = _LoadStatus;
}
