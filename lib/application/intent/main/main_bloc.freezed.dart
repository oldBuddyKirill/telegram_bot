// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'main_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
class _$MainEventTearOff {
  const _$MainEventTearOff();

  _LoadIntents loadIntents() {
    return const _LoadIntents();
  }

  _SaveChanges saveChanges() {
    return const _SaveChanges();
  }

  _IntentChanged intentChanged(String previousName, String nextName) {
    return _IntentChanged(
      previousName,
      nextName,
    );
  }

  _ExampleChanged exampleChanged(
      {required String example, required Intent intent, required int index}) {
    return _ExampleChanged(
      example: example,
      intent: intent,
      index: index,
    );
  }

  _AddIntent addIntent(Intent intent) {
    return _AddIntent(
      intent,
    );
  }

  _AddExample addExample(Intent intent) {
    return _AddExample(
      intent,
    );
  }

  _DeleteIntent deleteIntent(Intent intent) {
    return _DeleteIntent(
      intent,
    );
  }

  _DeleteExample deleteExample(
      {required int exampleIndex, required Intent intent}) {
    return _DeleteExample(
      exampleIndex: exampleIndex,
      intent: intent,
    );
  }

  _LoadStatus loadStatus() {
    return const _LoadStatus();
  }
}

/// @nodoc
const $MainEvent = _$MainEventTearOff();

/// @nodoc
mixin _$MainEvent {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() loadIntents,
    required TResult Function() saveChanges,
    required TResult Function(String previousName, String nextName)
        intentChanged,
    required TResult Function(String example, Intent intent, int index)
        exampleChanged,
    required TResult Function(Intent intent) addIntent,
    required TResult Function(Intent intent) addExample,
    required TResult Function(Intent intent) deleteIntent,
    required TResult Function(int exampleIndex, Intent intent) deleteExample,
    required TResult Function() loadStatus,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? loadIntents,
    TResult Function()? saveChanges,
    TResult Function(String previousName, String nextName)? intentChanged,
    TResult Function(String example, Intent intent, int index)? exampleChanged,
    TResult Function(Intent intent)? addIntent,
    TResult Function(Intent intent)? addExample,
    TResult Function(Intent intent)? deleteIntent,
    TResult Function(int exampleIndex, Intent intent)? deleteExample,
    TResult Function()? loadStatus,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? loadIntents,
    TResult Function()? saveChanges,
    TResult Function(String previousName, String nextName)? intentChanged,
    TResult Function(String example, Intent intent, int index)? exampleChanged,
    TResult Function(Intent intent)? addIntent,
    TResult Function(Intent intent)? addExample,
    TResult Function(Intent intent)? deleteIntent,
    TResult Function(int exampleIndex, Intent intent)? deleteExample,
    TResult Function()? loadStatus,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_LoadIntents value) loadIntents,
    required TResult Function(_SaveChanges value) saveChanges,
    required TResult Function(_IntentChanged value) intentChanged,
    required TResult Function(_ExampleChanged value) exampleChanged,
    required TResult Function(_AddIntent value) addIntent,
    required TResult Function(_AddExample value) addExample,
    required TResult Function(_DeleteIntent value) deleteIntent,
    required TResult Function(_DeleteExample value) deleteExample,
    required TResult Function(_LoadStatus value) loadStatus,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_LoadIntents value)? loadIntents,
    TResult Function(_SaveChanges value)? saveChanges,
    TResult Function(_IntentChanged value)? intentChanged,
    TResult Function(_ExampleChanged value)? exampleChanged,
    TResult Function(_AddIntent value)? addIntent,
    TResult Function(_AddExample value)? addExample,
    TResult Function(_DeleteIntent value)? deleteIntent,
    TResult Function(_DeleteExample value)? deleteExample,
    TResult Function(_LoadStatus value)? loadStatus,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_LoadIntents value)? loadIntents,
    TResult Function(_SaveChanges value)? saveChanges,
    TResult Function(_IntentChanged value)? intentChanged,
    TResult Function(_ExampleChanged value)? exampleChanged,
    TResult Function(_AddIntent value)? addIntent,
    TResult Function(_AddExample value)? addExample,
    TResult Function(_DeleteIntent value)? deleteIntent,
    TResult Function(_DeleteExample value)? deleteExample,
    TResult Function(_LoadStatus value)? loadStatus,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $MainEventCopyWith<$Res> {
  factory $MainEventCopyWith(MainEvent value, $Res Function(MainEvent) then) =
      _$MainEventCopyWithImpl<$Res>;
}

/// @nodoc
class _$MainEventCopyWithImpl<$Res> implements $MainEventCopyWith<$Res> {
  _$MainEventCopyWithImpl(this._value, this._then);

  final MainEvent _value;
  // ignore: unused_field
  final $Res Function(MainEvent) _then;
}

/// @nodoc
abstract class _$LoadIntentsCopyWith<$Res> {
  factory _$LoadIntentsCopyWith(
          _LoadIntents value, $Res Function(_LoadIntents) then) =
      __$LoadIntentsCopyWithImpl<$Res>;
}

/// @nodoc
class __$LoadIntentsCopyWithImpl<$Res> extends _$MainEventCopyWithImpl<$Res>
    implements _$LoadIntentsCopyWith<$Res> {
  __$LoadIntentsCopyWithImpl(
      _LoadIntents _value, $Res Function(_LoadIntents) _then)
      : super(_value, (v) => _then(v as _LoadIntents));

  @override
  _LoadIntents get _value => super._value as _LoadIntents;
}

/// @nodoc

class _$_LoadIntents implements _LoadIntents {
  const _$_LoadIntents();

  @override
  String toString() {
    return 'MainEvent.loadIntents()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _LoadIntents);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() loadIntents,
    required TResult Function() saveChanges,
    required TResult Function(String previousName, String nextName)
        intentChanged,
    required TResult Function(String example, Intent intent, int index)
        exampleChanged,
    required TResult Function(Intent intent) addIntent,
    required TResult Function(Intent intent) addExample,
    required TResult Function(Intent intent) deleteIntent,
    required TResult Function(int exampleIndex, Intent intent) deleteExample,
    required TResult Function() loadStatus,
  }) {
    return loadIntents();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? loadIntents,
    TResult Function()? saveChanges,
    TResult Function(String previousName, String nextName)? intentChanged,
    TResult Function(String example, Intent intent, int index)? exampleChanged,
    TResult Function(Intent intent)? addIntent,
    TResult Function(Intent intent)? addExample,
    TResult Function(Intent intent)? deleteIntent,
    TResult Function(int exampleIndex, Intent intent)? deleteExample,
    TResult Function()? loadStatus,
  }) {
    return loadIntents?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? loadIntents,
    TResult Function()? saveChanges,
    TResult Function(String previousName, String nextName)? intentChanged,
    TResult Function(String example, Intent intent, int index)? exampleChanged,
    TResult Function(Intent intent)? addIntent,
    TResult Function(Intent intent)? addExample,
    TResult Function(Intent intent)? deleteIntent,
    TResult Function(int exampleIndex, Intent intent)? deleteExample,
    TResult Function()? loadStatus,
    required TResult orElse(),
  }) {
    if (loadIntents != null) {
      return loadIntents();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_LoadIntents value) loadIntents,
    required TResult Function(_SaveChanges value) saveChanges,
    required TResult Function(_IntentChanged value) intentChanged,
    required TResult Function(_ExampleChanged value) exampleChanged,
    required TResult Function(_AddIntent value) addIntent,
    required TResult Function(_AddExample value) addExample,
    required TResult Function(_DeleteIntent value) deleteIntent,
    required TResult Function(_DeleteExample value) deleteExample,
    required TResult Function(_LoadStatus value) loadStatus,
  }) {
    return loadIntents(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_LoadIntents value)? loadIntents,
    TResult Function(_SaveChanges value)? saveChanges,
    TResult Function(_IntentChanged value)? intentChanged,
    TResult Function(_ExampleChanged value)? exampleChanged,
    TResult Function(_AddIntent value)? addIntent,
    TResult Function(_AddExample value)? addExample,
    TResult Function(_DeleteIntent value)? deleteIntent,
    TResult Function(_DeleteExample value)? deleteExample,
    TResult Function(_LoadStatus value)? loadStatus,
  }) {
    return loadIntents?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_LoadIntents value)? loadIntents,
    TResult Function(_SaveChanges value)? saveChanges,
    TResult Function(_IntentChanged value)? intentChanged,
    TResult Function(_ExampleChanged value)? exampleChanged,
    TResult Function(_AddIntent value)? addIntent,
    TResult Function(_AddExample value)? addExample,
    TResult Function(_DeleteIntent value)? deleteIntent,
    TResult Function(_DeleteExample value)? deleteExample,
    TResult Function(_LoadStatus value)? loadStatus,
    required TResult orElse(),
  }) {
    if (loadIntents != null) {
      return loadIntents(this);
    }
    return orElse();
  }
}

abstract class _LoadIntents implements MainEvent {
  const factory _LoadIntents() = _$_LoadIntents;
}

/// @nodoc
abstract class _$SaveChangesCopyWith<$Res> {
  factory _$SaveChangesCopyWith(
          _SaveChanges value, $Res Function(_SaveChanges) then) =
      __$SaveChangesCopyWithImpl<$Res>;
}

/// @nodoc
class __$SaveChangesCopyWithImpl<$Res> extends _$MainEventCopyWithImpl<$Res>
    implements _$SaveChangesCopyWith<$Res> {
  __$SaveChangesCopyWithImpl(
      _SaveChanges _value, $Res Function(_SaveChanges) _then)
      : super(_value, (v) => _then(v as _SaveChanges));

  @override
  _SaveChanges get _value => super._value as _SaveChanges;
}

/// @nodoc

class _$_SaveChanges implements _SaveChanges {
  const _$_SaveChanges();

  @override
  String toString() {
    return 'MainEvent.saveChanges()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _SaveChanges);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() loadIntents,
    required TResult Function() saveChanges,
    required TResult Function(String previousName, String nextName)
        intentChanged,
    required TResult Function(String example, Intent intent, int index)
        exampleChanged,
    required TResult Function(Intent intent) addIntent,
    required TResult Function(Intent intent) addExample,
    required TResult Function(Intent intent) deleteIntent,
    required TResult Function(int exampleIndex, Intent intent) deleteExample,
    required TResult Function() loadStatus,
  }) {
    return saveChanges();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? loadIntents,
    TResult Function()? saveChanges,
    TResult Function(String previousName, String nextName)? intentChanged,
    TResult Function(String example, Intent intent, int index)? exampleChanged,
    TResult Function(Intent intent)? addIntent,
    TResult Function(Intent intent)? addExample,
    TResult Function(Intent intent)? deleteIntent,
    TResult Function(int exampleIndex, Intent intent)? deleteExample,
    TResult Function()? loadStatus,
  }) {
    return saveChanges?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? loadIntents,
    TResult Function()? saveChanges,
    TResult Function(String previousName, String nextName)? intentChanged,
    TResult Function(String example, Intent intent, int index)? exampleChanged,
    TResult Function(Intent intent)? addIntent,
    TResult Function(Intent intent)? addExample,
    TResult Function(Intent intent)? deleteIntent,
    TResult Function(int exampleIndex, Intent intent)? deleteExample,
    TResult Function()? loadStatus,
    required TResult orElse(),
  }) {
    if (saveChanges != null) {
      return saveChanges();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_LoadIntents value) loadIntents,
    required TResult Function(_SaveChanges value) saveChanges,
    required TResult Function(_IntentChanged value) intentChanged,
    required TResult Function(_ExampleChanged value) exampleChanged,
    required TResult Function(_AddIntent value) addIntent,
    required TResult Function(_AddExample value) addExample,
    required TResult Function(_DeleteIntent value) deleteIntent,
    required TResult Function(_DeleteExample value) deleteExample,
    required TResult Function(_LoadStatus value) loadStatus,
  }) {
    return saveChanges(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_LoadIntents value)? loadIntents,
    TResult Function(_SaveChanges value)? saveChanges,
    TResult Function(_IntentChanged value)? intentChanged,
    TResult Function(_ExampleChanged value)? exampleChanged,
    TResult Function(_AddIntent value)? addIntent,
    TResult Function(_AddExample value)? addExample,
    TResult Function(_DeleteIntent value)? deleteIntent,
    TResult Function(_DeleteExample value)? deleteExample,
    TResult Function(_LoadStatus value)? loadStatus,
  }) {
    return saveChanges?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_LoadIntents value)? loadIntents,
    TResult Function(_SaveChanges value)? saveChanges,
    TResult Function(_IntentChanged value)? intentChanged,
    TResult Function(_ExampleChanged value)? exampleChanged,
    TResult Function(_AddIntent value)? addIntent,
    TResult Function(_AddExample value)? addExample,
    TResult Function(_DeleteIntent value)? deleteIntent,
    TResult Function(_DeleteExample value)? deleteExample,
    TResult Function(_LoadStatus value)? loadStatus,
    required TResult orElse(),
  }) {
    if (saveChanges != null) {
      return saveChanges(this);
    }
    return orElse();
  }
}

abstract class _SaveChanges implements MainEvent {
  const factory _SaveChanges() = _$_SaveChanges;
}

/// @nodoc
abstract class _$IntentChangedCopyWith<$Res> {
  factory _$IntentChangedCopyWith(
          _IntentChanged value, $Res Function(_IntentChanged) then) =
      __$IntentChangedCopyWithImpl<$Res>;
  $Res call({String previousName, String nextName});
}

/// @nodoc
class __$IntentChangedCopyWithImpl<$Res> extends _$MainEventCopyWithImpl<$Res>
    implements _$IntentChangedCopyWith<$Res> {
  __$IntentChangedCopyWithImpl(
      _IntentChanged _value, $Res Function(_IntentChanged) _then)
      : super(_value, (v) => _then(v as _IntentChanged));

  @override
  _IntentChanged get _value => super._value as _IntentChanged;

  @override
  $Res call({
    Object? previousName = freezed,
    Object? nextName = freezed,
  }) {
    return _then(_IntentChanged(
      previousName == freezed
          ? _value.previousName
          : previousName // ignore: cast_nullable_to_non_nullable
              as String,
      nextName == freezed
          ? _value.nextName
          : nextName // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$_IntentChanged implements _IntentChanged {
  const _$_IntentChanged(this.previousName, this.nextName);

  @override
  final String previousName;
  @override
  final String nextName;

  @override
  String toString() {
    return 'MainEvent.intentChanged(previousName: $previousName, nextName: $nextName)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _IntentChanged &&
            const DeepCollectionEquality()
                .equals(other.previousName, previousName) &&
            const DeepCollectionEquality().equals(other.nextName, nextName));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(previousName),
      const DeepCollectionEquality().hash(nextName));

  @JsonKey(ignore: true)
  @override
  _$IntentChangedCopyWith<_IntentChanged> get copyWith =>
      __$IntentChangedCopyWithImpl<_IntentChanged>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() loadIntents,
    required TResult Function() saveChanges,
    required TResult Function(String previousName, String nextName)
        intentChanged,
    required TResult Function(String example, Intent intent, int index)
        exampleChanged,
    required TResult Function(Intent intent) addIntent,
    required TResult Function(Intent intent) addExample,
    required TResult Function(Intent intent) deleteIntent,
    required TResult Function(int exampleIndex, Intent intent) deleteExample,
    required TResult Function() loadStatus,
  }) {
    return intentChanged(previousName, nextName);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? loadIntents,
    TResult Function()? saveChanges,
    TResult Function(String previousName, String nextName)? intentChanged,
    TResult Function(String example, Intent intent, int index)? exampleChanged,
    TResult Function(Intent intent)? addIntent,
    TResult Function(Intent intent)? addExample,
    TResult Function(Intent intent)? deleteIntent,
    TResult Function(int exampleIndex, Intent intent)? deleteExample,
    TResult Function()? loadStatus,
  }) {
    return intentChanged?.call(previousName, nextName);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? loadIntents,
    TResult Function()? saveChanges,
    TResult Function(String previousName, String nextName)? intentChanged,
    TResult Function(String example, Intent intent, int index)? exampleChanged,
    TResult Function(Intent intent)? addIntent,
    TResult Function(Intent intent)? addExample,
    TResult Function(Intent intent)? deleteIntent,
    TResult Function(int exampleIndex, Intent intent)? deleteExample,
    TResult Function()? loadStatus,
    required TResult orElse(),
  }) {
    if (intentChanged != null) {
      return intentChanged(previousName, nextName);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_LoadIntents value) loadIntents,
    required TResult Function(_SaveChanges value) saveChanges,
    required TResult Function(_IntentChanged value) intentChanged,
    required TResult Function(_ExampleChanged value) exampleChanged,
    required TResult Function(_AddIntent value) addIntent,
    required TResult Function(_AddExample value) addExample,
    required TResult Function(_DeleteIntent value) deleteIntent,
    required TResult Function(_DeleteExample value) deleteExample,
    required TResult Function(_LoadStatus value) loadStatus,
  }) {
    return intentChanged(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_LoadIntents value)? loadIntents,
    TResult Function(_SaveChanges value)? saveChanges,
    TResult Function(_IntentChanged value)? intentChanged,
    TResult Function(_ExampleChanged value)? exampleChanged,
    TResult Function(_AddIntent value)? addIntent,
    TResult Function(_AddExample value)? addExample,
    TResult Function(_DeleteIntent value)? deleteIntent,
    TResult Function(_DeleteExample value)? deleteExample,
    TResult Function(_LoadStatus value)? loadStatus,
  }) {
    return intentChanged?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_LoadIntents value)? loadIntents,
    TResult Function(_SaveChanges value)? saveChanges,
    TResult Function(_IntentChanged value)? intentChanged,
    TResult Function(_ExampleChanged value)? exampleChanged,
    TResult Function(_AddIntent value)? addIntent,
    TResult Function(_AddExample value)? addExample,
    TResult Function(_DeleteIntent value)? deleteIntent,
    TResult Function(_DeleteExample value)? deleteExample,
    TResult Function(_LoadStatus value)? loadStatus,
    required TResult orElse(),
  }) {
    if (intentChanged != null) {
      return intentChanged(this);
    }
    return orElse();
  }
}

abstract class _IntentChanged implements MainEvent {
  const factory _IntentChanged(String previousName, String nextName) =
      _$_IntentChanged;

  String get previousName;
  String get nextName;
  @JsonKey(ignore: true)
  _$IntentChangedCopyWith<_IntentChanged> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$ExampleChangedCopyWith<$Res> {
  factory _$ExampleChangedCopyWith(
          _ExampleChanged value, $Res Function(_ExampleChanged) then) =
      __$ExampleChangedCopyWithImpl<$Res>;
  $Res call({String example, Intent intent, int index});

  $IntentCopyWith<$Res> get intent;
}

/// @nodoc
class __$ExampleChangedCopyWithImpl<$Res> extends _$MainEventCopyWithImpl<$Res>
    implements _$ExampleChangedCopyWith<$Res> {
  __$ExampleChangedCopyWithImpl(
      _ExampleChanged _value, $Res Function(_ExampleChanged) _then)
      : super(_value, (v) => _then(v as _ExampleChanged));

  @override
  _ExampleChanged get _value => super._value as _ExampleChanged;

  @override
  $Res call({
    Object? example = freezed,
    Object? intent = freezed,
    Object? index = freezed,
  }) {
    return _then(_ExampleChanged(
      example: example == freezed
          ? _value.example
          : example // ignore: cast_nullable_to_non_nullable
              as String,
      intent: intent == freezed
          ? _value.intent
          : intent // ignore: cast_nullable_to_non_nullable
              as Intent,
      index: index == freezed
          ? _value.index
          : index // ignore: cast_nullable_to_non_nullable
              as int,
    ));
  }

  @override
  $IntentCopyWith<$Res> get intent {
    return $IntentCopyWith<$Res>(_value.intent, (value) {
      return _then(_value.copyWith(intent: value));
    });
  }
}

/// @nodoc

class _$_ExampleChanged implements _ExampleChanged {
  const _$_ExampleChanged(
      {required this.example, required this.intent, required this.index});

  @override
  final String example;
  @override
  final Intent intent;
  @override
  final int index;

  @override
  String toString() {
    return 'MainEvent.exampleChanged(example: $example, intent: $intent, index: $index)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _ExampleChanged &&
            const DeepCollectionEquality().equals(other.example, example) &&
            const DeepCollectionEquality().equals(other.intent, intent) &&
            const DeepCollectionEquality().equals(other.index, index));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(example),
      const DeepCollectionEquality().hash(intent),
      const DeepCollectionEquality().hash(index));

  @JsonKey(ignore: true)
  @override
  _$ExampleChangedCopyWith<_ExampleChanged> get copyWith =>
      __$ExampleChangedCopyWithImpl<_ExampleChanged>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() loadIntents,
    required TResult Function() saveChanges,
    required TResult Function(String previousName, String nextName)
        intentChanged,
    required TResult Function(String example, Intent intent, int index)
        exampleChanged,
    required TResult Function(Intent intent) addIntent,
    required TResult Function(Intent intent) addExample,
    required TResult Function(Intent intent) deleteIntent,
    required TResult Function(int exampleIndex, Intent intent) deleteExample,
    required TResult Function() loadStatus,
  }) {
    return exampleChanged(example, intent, index);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? loadIntents,
    TResult Function()? saveChanges,
    TResult Function(String previousName, String nextName)? intentChanged,
    TResult Function(String example, Intent intent, int index)? exampleChanged,
    TResult Function(Intent intent)? addIntent,
    TResult Function(Intent intent)? addExample,
    TResult Function(Intent intent)? deleteIntent,
    TResult Function(int exampleIndex, Intent intent)? deleteExample,
    TResult Function()? loadStatus,
  }) {
    return exampleChanged?.call(example, intent, index);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? loadIntents,
    TResult Function()? saveChanges,
    TResult Function(String previousName, String nextName)? intentChanged,
    TResult Function(String example, Intent intent, int index)? exampleChanged,
    TResult Function(Intent intent)? addIntent,
    TResult Function(Intent intent)? addExample,
    TResult Function(Intent intent)? deleteIntent,
    TResult Function(int exampleIndex, Intent intent)? deleteExample,
    TResult Function()? loadStatus,
    required TResult orElse(),
  }) {
    if (exampleChanged != null) {
      return exampleChanged(example, intent, index);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_LoadIntents value) loadIntents,
    required TResult Function(_SaveChanges value) saveChanges,
    required TResult Function(_IntentChanged value) intentChanged,
    required TResult Function(_ExampleChanged value) exampleChanged,
    required TResult Function(_AddIntent value) addIntent,
    required TResult Function(_AddExample value) addExample,
    required TResult Function(_DeleteIntent value) deleteIntent,
    required TResult Function(_DeleteExample value) deleteExample,
    required TResult Function(_LoadStatus value) loadStatus,
  }) {
    return exampleChanged(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_LoadIntents value)? loadIntents,
    TResult Function(_SaveChanges value)? saveChanges,
    TResult Function(_IntentChanged value)? intentChanged,
    TResult Function(_ExampleChanged value)? exampleChanged,
    TResult Function(_AddIntent value)? addIntent,
    TResult Function(_AddExample value)? addExample,
    TResult Function(_DeleteIntent value)? deleteIntent,
    TResult Function(_DeleteExample value)? deleteExample,
    TResult Function(_LoadStatus value)? loadStatus,
  }) {
    return exampleChanged?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_LoadIntents value)? loadIntents,
    TResult Function(_SaveChanges value)? saveChanges,
    TResult Function(_IntentChanged value)? intentChanged,
    TResult Function(_ExampleChanged value)? exampleChanged,
    TResult Function(_AddIntent value)? addIntent,
    TResult Function(_AddExample value)? addExample,
    TResult Function(_DeleteIntent value)? deleteIntent,
    TResult Function(_DeleteExample value)? deleteExample,
    TResult Function(_LoadStatus value)? loadStatus,
    required TResult orElse(),
  }) {
    if (exampleChanged != null) {
      return exampleChanged(this);
    }
    return orElse();
  }
}

abstract class _ExampleChanged implements MainEvent {
  const factory _ExampleChanged(
      {required String example,
      required Intent intent,
      required int index}) = _$_ExampleChanged;

  String get example;
  Intent get intent;
  int get index;
  @JsonKey(ignore: true)
  _$ExampleChangedCopyWith<_ExampleChanged> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$AddIntentCopyWith<$Res> {
  factory _$AddIntentCopyWith(
          _AddIntent value, $Res Function(_AddIntent) then) =
      __$AddIntentCopyWithImpl<$Res>;
  $Res call({Intent intent});

  $IntentCopyWith<$Res> get intent;
}

/// @nodoc
class __$AddIntentCopyWithImpl<$Res> extends _$MainEventCopyWithImpl<$Res>
    implements _$AddIntentCopyWith<$Res> {
  __$AddIntentCopyWithImpl(_AddIntent _value, $Res Function(_AddIntent) _then)
      : super(_value, (v) => _then(v as _AddIntent));

  @override
  _AddIntent get _value => super._value as _AddIntent;

  @override
  $Res call({
    Object? intent = freezed,
  }) {
    return _then(_AddIntent(
      intent == freezed
          ? _value.intent
          : intent // ignore: cast_nullable_to_non_nullable
              as Intent,
    ));
  }

  @override
  $IntentCopyWith<$Res> get intent {
    return $IntentCopyWith<$Res>(_value.intent, (value) {
      return _then(_value.copyWith(intent: value));
    });
  }
}

/// @nodoc

class _$_AddIntent implements _AddIntent {
  const _$_AddIntent(this.intent);

  @override
  final Intent intent;

  @override
  String toString() {
    return 'MainEvent.addIntent(intent: $intent)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _AddIntent &&
            const DeepCollectionEquality().equals(other.intent, intent));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(intent));

  @JsonKey(ignore: true)
  @override
  _$AddIntentCopyWith<_AddIntent> get copyWith =>
      __$AddIntentCopyWithImpl<_AddIntent>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() loadIntents,
    required TResult Function() saveChanges,
    required TResult Function(String previousName, String nextName)
        intentChanged,
    required TResult Function(String example, Intent intent, int index)
        exampleChanged,
    required TResult Function(Intent intent) addIntent,
    required TResult Function(Intent intent) addExample,
    required TResult Function(Intent intent) deleteIntent,
    required TResult Function(int exampleIndex, Intent intent) deleteExample,
    required TResult Function() loadStatus,
  }) {
    return addIntent(intent);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? loadIntents,
    TResult Function()? saveChanges,
    TResult Function(String previousName, String nextName)? intentChanged,
    TResult Function(String example, Intent intent, int index)? exampleChanged,
    TResult Function(Intent intent)? addIntent,
    TResult Function(Intent intent)? addExample,
    TResult Function(Intent intent)? deleteIntent,
    TResult Function(int exampleIndex, Intent intent)? deleteExample,
    TResult Function()? loadStatus,
  }) {
    return addIntent?.call(intent);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? loadIntents,
    TResult Function()? saveChanges,
    TResult Function(String previousName, String nextName)? intentChanged,
    TResult Function(String example, Intent intent, int index)? exampleChanged,
    TResult Function(Intent intent)? addIntent,
    TResult Function(Intent intent)? addExample,
    TResult Function(Intent intent)? deleteIntent,
    TResult Function(int exampleIndex, Intent intent)? deleteExample,
    TResult Function()? loadStatus,
    required TResult orElse(),
  }) {
    if (addIntent != null) {
      return addIntent(intent);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_LoadIntents value) loadIntents,
    required TResult Function(_SaveChanges value) saveChanges,
    required TResult Function(_IntentChanged value) intentChanged,
    required TResult Function(_ExampleChanged value) exampleChanged,
    required TResult Function(_AddIntent value) addIntent,
    required TResult Function(_AddExample value) addExample,
    required TResult Function(_DeleteIntent value) deleteIntent,
    required TResult Function(_DeleteExample value) deleteExample,
    required TResult Function(_LoadStatus value) loadStatus,
  }) {
    return addIntent(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_LoadIntents value)? loadIntents,
    TResult Function(_SaveChanges value)? saveChanges,
    TResult Function(_IntentChanged value)? intentChanged,
    TResult Function(_ExampleChanged value)? exampleChanged,
    TResult Function(_AddIntent value)? addIntent,
    TResult Function(_AddExample value)? addExample,
    TResult Function(_DeleteIntent value)? deleteIntent,
    TResult Function(_DeleteExample value)? deleteExample,
    TResult Function(_LoadStatus value)? loadStatus,
  }) {
    return addIntent?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_LoadIntents value)? loadIntents,
    TResult Function(_SaveChanges value)? saveChanges,
    TResult Function(_IntentChanged value)? intentChanged,
    TResult Function(_ExampleChanged value)? exampleChanged,
    TResult Function(_AddIntent value)? addIntent,
    TResult Function(_AddExample value)? addExample,
    TResult Function(_DeleteIntent value)? deleteIntent,
    TResult Function(_DeleteExample value)? deleteExample,
    TResult Function(_LoadStatus value)? loadStatus,
    required TResult orElse(),
  }) {
    if (addIntent != null) {
      return addIntent(this);
    }
    return orElse();
  }
}

abstract class _AddIntent implements MainEvent {
  const factory _AddIntent(Intent intent) = _$_AddIntent;

  Intent get intent;
  @JsonKey(ignore: true)
  _$AddIntentCopyWith<_AddIntent> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$AddExampleCopyWith<$Res> {
  factory _$AddExampleCopyWith(
          _AddExample value, $Res Function(_AddExample) then) =
      __$AddExampleCopyWithImpl<$Res>;
  $Res call({Intent intent});

  $IntentCopyWith<$Res> get intent;
}

/// @nodoc
class __$AddExampleCopyWithImpl<$Res> extends _$MainEventCopyWithImpl<$Res>
    implements _$AddExampleCopyWith<$Res> {
  __$AddExampleCopyWithImpl(
      _AddExample _value, $Res Function(_AddExample) _then)
      : super(_value, (v) => _then(v as _AddExample));

  @override
  _AddExample get _value => super._value as _AddExample;

  @override
  $Res call({
    Object? intent = freezed,
  }) {
    return _then(_AddExample(
      intent == freezed
          ? _value.intent
          : intent // ignore: cast_nullable_to_non_nullable
              as Intent,
    ));
  }

  @override
  $IntentCopyWith<$Res> get intent {
    return $IntentCopyWith<$Res>(_value.intent, (value) {
      return _then(_value.copyWith(intent: value));
    });
  }
}

/// @nodoc

class _$_AddExample implements _AddExample {
  const _$_AddExample(this.intent);

  @override
  final Intent intent;

  @override
  String toString() {
    return 'MainEvent.addExample(intent: $intent)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _AddExample &&
            const DeepCollectionEquality().equals(other.intent, intent));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(intent));

  @JsonKey(ignore: true)
  @override
  _$AddExampleCopyWith<_AddExample> get copyWith =>
      __$AddExampleCopyWithImpl<_AddExample>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() loadIntents,
    required TResult Function() saveChanges,
    required TResult Function(String previousName, String nextName)
        intentChanged,
    required TResult Function(String example, Intent intent, int index)
        exampleChanged,
    required TResult Function(Intent intent) addIntent,
    required TResult Function(Intent intent) addExample,
    required TResult Function(Intent intent) deleteIntent,
    required TResult Function(int exampleIndex, Intent intent) deleteExample,
    required TResult Function() loadStatus,
  }) {
    return addExample(intent);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? loadIntents,
    TResult Function()? saveChanges,
    TResult Function(String previousName, String nextName)? intentChanged,
    TResult Function(String example, Intent intent, int index)? exampleChanged,
    TResult Function(Intent intent)? addIntent,
    TResult Function(Intent intent)? addExample,
    TResult Function(Intent intent)? deleteIntent,
    TResult Function(int exampleIndex, Intent intent)? deleteExample,
    TResult Function()? loadStatus,
  }) {
    return addExample?.call(intent);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? loadIntents,
    TResult Function()? saveChanges,
    TResult Function(String previousName, String nextName)? intentChanged,
    TResult Function(String example, Intent intent, int index)? exampleChanged,
    TResult Function(Intent intent)? addIntent,
    TResult Function(Intent intent)? addExample,
    TResult Function(Intent intent)? deleteIntent,
    TResult Function(int exampleIndex, Intent intent)? deleteExample,
    TResult Function()? loadStatus,
    required TResult orElse(),
  }) {
    if (addExample != null) {
      return addExample(intent);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_LoadIntents value) loadIntents,
    required TResult Function(_SaveChanges value) saveChanges,
    required TResult Function(_IntentChanged value) intentChanged,
    required TResult Function(_ExampleChanged value) exampleChanged,
    required TResult Function(_AddIntent value) addIntent,
    required TResult Function(_AddExample value) addExample,
    required TResult Function(_DeleteIntent value) deleteIntent,
    required TResult Function(_DeleteExample value) deleteExample,
    required TResult Function(_LoadStatus value) loadStatus,
  }) {
    return addExample(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_LoadIntents value)? loadIntents,
    TResult Function(_SaveChanges value)? saveChanges,
    TResult Function(_IntentChanged value)? intentChanged,
    TResult Function(_ExampleChanged value)? exampleChanged,
    TResult Function(_AddIntent value)? addIntent,
    TResult Function(_AddExample value)? addExample,
    TResult Function(_DeleteIntent value)? deleteIntent,
    TResult Function(_DeleteExample value)? deleteExample,
    TResult Function(_LoadStatus value)? loadStatus,
  }) {
    return addExample?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_LoadIntents value)? loadIntents,
    TResult Function(_SaveChanges value)? saveChanges,
    TResult Function(_IntentChanged value)? intentChanged,
    TResult Function(_ExampleChanged value)? exampleChanged,
    TResult Function(_AddIntent value)? addIntent,
    TResult Function(_AddExample value)? addExample,
    TResult Function(_DeleteIntent value)? deleteIntent,
    TResult Function(_DeleteExample value)? deleteExample,
    TResult Function(_LoadStatus value)? loadStatus,
    required TResult orElse(),
  }) {
    if (addExample != null) {
      return addExample(this);
    }
    return orElse();
  }
}

abstract class _AddExample implements MainEvent {
  const factory _AddExample(Intent intent) = _$_AddExample;

  Intent get intent;
  @JsonKey(ignore: true)
  _$AddExampleCopyWith<_AddExample> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$DeleteIntentCopyWith<$Res> {
  factory _$DeleteIntentCopyWith(
          _DeleteIntent value, $Res Function(_DeleteIntent) then) =
      __$DeleteIntentCopyWithImpl<$Res>;
  $Res call({Intent intent});

  $IntentCopyWith<$Res> get intent;
}

/// @nodoc
class __$DeleteIntentCopyWithImpl<$Res> extends _$MainEventCopyWithImpl<$Res>
    implements _$DeleteIntentCopyWith<$Res> {
  __$DeleteIntentCopyWithImpl(
      _DeleteIntent _value, $Res Function(_DeleteIntent) _then)
      : super(_value, (v) => _then(v as _DeleteIntent));

  @override
  _DeleteIntent get _value => super._value as _DeleteIntent;

  @override
  $Res call({
    Object? intent = freezed,
  }) {
    return _then(_DeleteIntent(
      intent == freezed
          ? _value.intent
          : intent // ignore: cast_nullable_to_non_nullable
              as Intent,
    ));
  }

  @override
  $IntentCopyWith<$Res> get intent {
    return $IntentCopyWith<$Res>(_value.intent, (value) {
      return _then(_value.copyWith(intent: value));
    });
  }
}

/// @nodoc

class _$_DeleteIntent implements _DeleteIntent {
  const _$_DeleteIntent(this.intent);

  @override
  final Intent intent;

  @override
  String toString() {
    return 'MainEvent.deleteIntent(intent: $intent)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _DeleteIntent &&
            const DeepCollectionEquality().equals(other.intent, intent));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(intent));

  @JsonKey(ignore: true)
  @override
  _$DeleteIntentCopyWith<_DeleteIntent> get copyWith =>
      __$DeleteIntentCopyWithImpl<_DeleteIntent>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() loadIntents,
    required TResult Function() saveChanges,
    required TResult Function(String previousName, String nextName)
        intentChanged,
    required TResult Function(String example, Intent intent, int index)
        exampleChanged,
    required TResult Function(Intent intent) addIntent,
    required TResult Function(Intent intent) addExample,
    required TResult Function(Intent intent) deleteIntent,
    required TResult Function(int exampleIndex, Intent intent) deleteExample,
    required TResult Function() loadStatus,
  }) {
    return deleteIntent(intent);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? loadIntents,
    TResult Function()? saveChanges,
    TResult Function(String previousName, String nextName)? intentChanged,
    TResult Function(String example, Intent intent, int index)? exampleChanged,
    TResult Function(Intent intent)? addIntent,
    TResult Function(Intent intent)? addExample,
    TResult Function(Intent intent)? deleteIntent,
    TResult Function(int exampleIndex, Intent intent)? deleteExample,
    TResult Function()? loadStatus,
  }) {
    return deleteIntent?.call(intent);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? loadIntents,
    TResult Function()? saveChanges,
    TResult Function(String previousName, String nextName)? intentChanged,
    TResult Function(String example, Intent intent, int index)? exampleChanged,
    TResult Function(Intent intent)? addIntent,
    TResult Function(Intent intent)? addExample,
    TResult Function(Intent intent)? deleteIntent,
    TResult Function(int exampleIndex, Intent intent)? deleteExample,
    TResult Function()? loadStatus,
    required TResult orElse(),
  }) {
    if (deleteIntent != null) {
      return deleteIntent(intent);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_LoadIntents value) loadIntents,
    required TResult Function(_SaveChanges value) saveChanges,
    required TResult Function(_IntentChanged value) intentChanged,
    required TResult Function(_ExampleChanged value) exampleChanged,
    required TResult Function(_AddIntent value) addIntent,
    required TResult Function(_AddExample value) addExample,
    required TResult Function(_DeleteIntent value) deleteIntent,
    required TResult Function(_DeleteExample value) deleteExample,
    required TResult Function(_LoadStatus value) loadStatus,
  }) {
    return deleteIntent(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_LoadIntents value)? loadIntents,
    TResult Function(_SaveChanges value)? saveChanges,
    TResult Function(_IntentChanged value)? intentChanged,
    TResult Function(_ExampleChanged value)? exampleChanged,
    TResult Function(_AddIntent value)? addIntent,
    TResult Function(_AddExample value)? addExample,
    TResult Function(_DeleteIntent value)? deleteIntent,
    TResult Function(_DeleteExample value)? deleteExample,
    TResult Function(_LoadStatus value)? loadStatus,
  }) {
    return deleteIntent?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_LoadIntents value)? loadIntents,
    TResult Function(_SaveChanges value)? saveChanges,
    TResult Function(_IntentChanged value)? intentChanged,
    TResult Function(_ExampleChanged value)? exampleChanged,
    TResult Function(_AddIntent value)? addIntent,
    TResult Function(_AddExample value)? addExample,
    TResult Function(_DeleteIntent value)? deleteIntent,
    TResult Function(_DeleteExample value)? deleteExample,
    TResult Function(_LoadStatus value)? loadStatus,
    required TResult orElse(),
  }) {
    if (deleteIntent != null) {
      return deleteIntent(this);
    }
    return orElse();
  }
}

abstract class _DeleteIntent implements MainEvent {
  const factory _DeleteIntent(Intent intent) = _$_DeleteIntent;

  Intent get intent;
  @JsonKey(ignore: true)
  _$DeleteIntentCopyWith<_DeleteIntent> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$DeleteExampleCopyWith<$Res> {
  factory _$DeleteExampleCopyWith(
          _DeleteExample value, $Res Function(_DeleteExample) then) =
      __$DeleteExampleCopyWithImpl<$Res>;
  $Res call({int exampleIndex, Intent intent});

  $IntentCopyWith<$Res> get intent;
}

/// @nodoc
class __$DeleteExampleCopyWithImpl<$Res> extends _$MainEventCopyWithImpl<$Res>
    implements _$DeleteExampleCopyWith<$Res> {
  __$DeleteExampleCopyWithImpl(
      _DeleteExample _value, $Res Function(_DeleteExample) _then)
      : super(_value, (v) => _then(v as _DeleteExample));

  @override
  _DeleteExample get _value => super._value as _DeleteExample;

  @override
  $Res call({
    Object? exampleIndex = freezed,
    Object? intent = freezed,
  }) {
    return _then(_DeleteExample(
      exampleIndex: exampleIndex == freezed
          ? _value.exampleIndex
          : exampleIndex // ignore: cast_nullable_to_non_nullable
              as int,
      intent: intent == freezed
          ? _value.intent
          : intent // ignore: cast_nullable_to_non_nullable
              as Intent,
    ));
  }

  @override
  $IntentCopyWith<$Res> get intent {
    return $IntentCopyWith<$Res>(_value.intent, (value) {
      return _then(_value.copyWith(intent: value));
    });
  }
}

/// @nodoc

class _$_DeleteExample implements _DeleteExample {
  const _$_DeleteExample({required this.exampleIndex, required this.intent});

  @override
  final int exampleIndex;
  @override
  final Intent intent;

  @override
  String toString() {
    return 'MainEvent.deleteExample(exampleIndex: $exampleIndex, intent: $intent)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _DeleteExample &&
            const DeepCollectionEquality()
                .equals(other.exampleIndex, exampleIndex) &&
            const DeepCollectionEquality().equals(other.intent, intent));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(exampleIndex),
      const DeepCollectionEquality().hash(intent));

  @JsonKey(ignore: true)
  @override
  _$DeleteExampleCopyWith<_DeleteExample> get copyWith =>
      __$DeleteExampleCopyWithImpl<_DeleteExample>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() loadIntents,
    required TResult Function() saveChanges,
    required TResult Function(String previousName, String nextName)
        intentChanged,
    required TResult Function(String example, Intent intent, int index)
        exampleChanged,
    required TResult Function(Intent intent) addIntent,
    required TResult Function(Intent intent) addExample,
    required TResult Function(Intent intent) deleteIntent,
    required TResult Function(int exampleIndex, Intent intent) deleteExample,
    required TResult Function() loadStatus,
  }) {
    return deleteExample(exampleIndex, intent);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? loadIntents,
    TResult Function()? saveChanges,
    TResult Function(String previousName, String nextName)? intentChanged,
    TResult Function(String example, Intent intent, int index)? exampleChanged,
    TResult Function(Intent intent)? addIntent,
    TResult Function(Intent intent)? addExample,
    TResult Function(Intent intent)? deleteIntent,
    TResult Function(int exampleIndex, Intent intent)? deleteExample,
    TResult Function()? loadStatus,
  }) {
    return deleteExample?.call(exampleIndex, intent);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? loadIntents,
    TResult Function()? saveChanges,
    TResult Function(String previousName, String nextName)? intentChanged,
    TResult Function(String example, Intent intent, int index)? exampleChanged,
    TResult Function(Intent intent)? addIntent,
    TResult Function(Intent intent)? addExample,
    TResult Function(Intent intent)? deleteIntent,
    TResult Function(int exampleIndex, Intent intent)? deleteExample,
    TResult Function()? loadStatus,
    required TResult orElse(),
  }) {
    if (deleteExample != null) {
      return deleteExample(exampleIndex, intent);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_LoadIntents value) loadIntents,
    required TResult Function(_SaveChanges value) saveChanges,
    required TResult Function(_IntentChanged value) intentChanged,
    required TResult Function(_ExampleChanged value) exampleChanged,
    required TResult Function(_AddIntent value) addIntent,
    required TResult Function(_AddExample value) addExample,
    required TResult Function(_DeleteIntent value) deleteIntent,
    required TResult Function(_DeleteExample value) deleteExample,
    required TResult Function(_LoadStatus value) loadStatus,
  }) {
    return deleteExample(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_LoadIntents value)? loadIntents,
    TResult Function(_SaveChanges value)? saveChanges,
    TResult Function(_IntentChanged value)? intentChanged,
    TResult Function(_ExampleChanged value)? exampleChanged,
    TResult Function(_AddIntent value)? addIntent,
    TResult Function(_AddExample value)? addExample,
    TResult Function(_DeleteIntent value)? deleteIntent,
    TResult Function(_DeleteExample value)? deleteExample,
    TResult Function(_LoadStatus value)? loadStatus,
  }) {
    return deleteExample?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_LoadIntents value)? loadIntents,
    TResult Function(_SaveChanges value)? saveChanges,
    TResult Function(_IntentChanged value)? intentChanged,
    TResult Function(_ExampleChanged value)? exampleChanged,
    TResult Function(_AddIntent value)? addIntent,
    TResult Function(_AddExample value)? addExample,
    TResult Function(_DeleteIntent value)? deleteIntent,
    TResult Function(_DeleteExample value)? deleteExample,
    TResult Function(_LoadStatus value)? loadStatus,
    required TResult orElse(),
  }) {
    if (deleteExample != null) {
      return deleteExample(this);
    }
    return orElse();
  }
}

abstract class _DeleteExample implements MainEvent {
  const factory _DeleteExample(
      {required int exampleIndex, required Intent intent}) = _$_DeleteExample;

  int get exampleIndex;
  Intent get intent;
  @JsonKey(ignore: true)
  _$DeleteExampleCopyWith<_DeleteExample> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$LoadStatusCopyWith<$Res> {
  factory _$LoadStatusCopyWith(
          _LoadStatus value, $Res Function(_LoadStatus) then) =
      __$LoadStatusCopyWithImpl<$Res>;
}

/// @nodoc
class __$LoadStatusCopyWithImpl<$Res> extends _$MainEventCopyWithImpl<$Res>
    implements _$LoadStatusCopyWith<$Res> {
  __$LoadStatusCopyWithImpl(
      _LoadStatus _value, $Res Function(_LoadStatus) _then)
      : super(_value, (v) => _then(v as _LoadStatus));

  @override
  _LoadStatus get _value => super._value as _LoadStatus;
}

/// @nodoc

class _$_LoadStatus implements _LoadStatus {
  const _$_LoadStatus();

  @override
  String toString() {
    return 'MainEvent.loadStatus()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _LoadStatus);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() loadIntents,
    required TResult Function() saveChanges,
    required TResult Function(String previousName, String nextName)
        intentChanged,
    required TResult Function(String example, Intent intent, int index)
        exampleChanged,
    required TResult Function(Intent intent) addIntent,
    required TResult Function(Intent intent) addExample,
    required TResult Function(Intent intent) deleteIntent,
    required TResult Function(int exampleIndex, Intent intent) deleteExample,
    required TResult Function() loadStatus,
  }) {
    return loadStatus();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? loadIntents,
    TResult Function()? saveChanges,
    TResult Function(String previousName, String nextName)? intentChanged,
    TResult Function(String example, Intent intent, int index)? exampleChanged,
    TResult Function(Intent intent)? addIntent,
    TResult Function(Intent intent)? addExample,
    TResult Function(Intent intent)? deleteIntent,
    TResult Function(int exampleIndex, Intent intent)? deleteExample,
    TResult Function()? loadStatus,
  }) {
    return loadStatus?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? loadIntents,
    TResult Function()? saveChanges,
    TResult Function(String previousName, String nextName)? intentChanged,
    TResult Function(String example, Intent intent, int index)? exampleChanged,
    TResult Function(Intent intent)? addIntent,
    TResult Function(Intent intent)? addExample,
    TResult Function(Intent intent)? deleteIntent,
    TResult Function(int exampleIndex, Intent intent)? deleteExample,
    TResult Function()? loadStatus,
    required TResult orElse(),
  }) {
    if (loadStatus != null) {
      return loadStatus();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_LoadIntents value) loadIntents,
    required TResult Function(_SaveChanges value) saveChanges,
    required TResult Function(_IntentChanged value) intentChanged,
    required TResult Function(_ExampleChanged value) exampleChanged,
    required TResult Function(_AddIntent value) addIntent,
    required TResult Function(_AddExample value) addExample,
    required TResult Function(_DeleteIntent value) deleteIntent,
    required TResult Function(_DeleteExample value) deleteExample,
    required TResult Function(_LoadStatus value) loadStatus,
  }) {
    return loadStatus(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_LoadIntents value)? loadIntents,
    TResult Function(_SaveChanges value)? saveChanges,
    TResult Function(_IntentChanged value)? intentChanged,
    TResult Function(_ExampleChanged value)? exampleChanged,
    TResult Function(_AddIntent value)? addIntent,
    TResult Function(_AddExample value)? addExample,
    TResult Function(_DeleteIntent value)? deleteIntent,
    TResult Function(_DeleteExample value)? deleteExample,
    TResult Function(_LoadStatus value)? loadStatus,
  }) {
    return loadStatus?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_LoadIntents value)? loadIntents,
    TResult Function(_SaveChanges value)? saveChanges,
    TResult Function(_IntentChanged value)? intentChanged,
    TResult Function(_ExampleChanged value)? exampleChanged,
    TResult Function(_AddIntent value)? addIntent,
    TResult Function(_AddExample value)? addExample,
    TResult Function(_DeleteIntent value)? deleteIntent,
    TResult Function(_DeleteExample value)? deleteExample,
    TResult Function(_LoadStatus value)? loadStatus,
    required TResult orElse(),
  }) {
    if (loadStatus != null) {
      return loadStatus(this);
    }
    return orElse();
  }
}

abstract class _LoadStatus implements MainEvent {
  const factory _LoadStatus() = _$_LoadStatus;
}

/// @nodoc
class _$MainStateTearOff {
  const _$MainStateTearOff();

  _Initial initial() {
    return const _Initial();
  }

  _Loading loading() {
    return const _Loading();
  }

  _ShowScreen showScreen(List<Intent> intents, bool isEnabledToSave) {
    return _ShowScreen(
      intents,
      isEnabledToSave,
    );
  }

  _SaveChangesFailure saveChangesFailure(IntentFailure intentFailure) {
    return _SaveChangesFailure(
      intentFailure,
    );
  }

  _LoadFailure loadFailure(IntentFailure intentFailure) {
    return _LoadFailure(
      intentFailure,
    );
  }

  _SuccessfulDeletionIntent successfulDeletionIntent() {
    return const _SuccessfulDeletionIntent();
  }

  _SuccessfulDeletionExample successfulDeletionExample() {
    return const _SuccessfulDeletionExample();
  }

  _IntentAdded intentAdded() {
    return const _IntentAdded();
  }

  _ShowStatus showStatus(String status) {
    return _ShowStatus(
      status,
    );
  }

  _LoadStatusFailure loadStatusFailure() {
    return const _LoadStatusFailure();
  }

  _SuccessfulSavingData successfulSavingData() {
    return const _SuccessfulSavingData();
  }
}

/// @nodoc
const $MainState = _$MainStateTearOff();

/// @nodoc
mixin _$MainState {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(List<Intent> intents, bool isEnabledToSave)
        showScreen,
    required TResult Function(IntentFailure intentFailure) saveChangesFailure,
    required TResult Function(IntentFailure intentFailure) loadFailure,
    required TResult Function() successfulDeletionIntent,
    required TResult Function() successfulDeletionExample,
    required TResult Function() intentAdded,
    required TResult Function(String status) showStatus,
    required TResult Function() loadStatusFailure,
    required TResult Function() successfulSavingData,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(List<Intent> intents, bool isEnabledToSave)? showScreen,
    TResult Function(IntentFailure intentFailure)? saveChangesFailure,
    TResult Function(IntentFailure intentFailure)? loadFailure,
    TResult Function()? successfulDeletionIntent,
    TResult Function()? successfulDeletionExample,
    TResult Function()? intentAdded,
    TResult Function(String status)? showStatus,
    TResult Function()? loadStatusFailure,
    TResult Function()? successfulSavingData,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(List<Intent> intents, bool isEnabledToSave)? showScreen,
    TResult Function(IntentFailure intentFailure)? saveChangesFailure,
    TResult Function(IntentFailure intentFailure)? loadFailure,
    TResult Function()? successfulDeletionIntent,
    TResult Function()? successfulDeletionExample,
    TResult Function()? intentAdded,
    TResult Function(String status)? showStatus,
    TResult Function()? loadStatusFailure,
    TResult Function()? successfulSavingData,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_Loading value) loading,
    required TResult Function(_ShowScreen value) showScreen,
    required TResult Function(_SaveChangesFailure value) saveChangesFailure,
    required TResult Function(_LoadFailure value) loadFailure,
    required TResult Function(_SuccessfulDeletionIntent value)
        successfulDeletionIntent,
    required TResult Function(_SuccessfulDeletionExample value)
        successfulDeletionExample,
    required TResult Function(_IntentAdded value) intentAdded,
    required TResult Function(_ShowStatus value) showStatus,
    required TResult Function(_LoadStatusFailure value) loadStatusFailure,
    required TResult Function(_SuccessfulSavingData value) successfulSavingData,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_Loading value)? loading,
    TResult Function(_ShowScreen value)? showScreen,
    TResult Function(_SaveChangesFailure value)? saveChangesFailure,
    TResult Function(_LoadFailure value)? loadFailure,
    TResult Function(_SuccessfulDeletionIntent value)? successfulDeletionIntent,
    TResult Function(_SuccessfulDeletionExample value)?
        successfulDeletionExample,
    TResult Function(_IntentAdded value)? intentAdded,
    TResult Function(_ShowStatus value)? showStatus,
    TResult Function(_LoadStatusFailure value)? loadStatusFailure,
    TResult Function(_SuccessfulSavingData value)? successfulSavingData,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_Loading value)? loading,
    TResult Function(_ShowScreen value)? showScreen,
    TResult Function(_SaveChangesFailure value)? saveChangesFailure,
    TResult Function(_LoadFailure value)? loadFailure,
    TResult Function(_SuccessfulDeletionIntent value)? successfulDeletionIntent,
    TResult Function(_SuccessfulDeletionExample value)?
        successfulDeletionExample,
    TResult Function(_IntentAdded value)? intentAdded,
    TResult Function(_ShowStatus value)? showStatus,
    TResult Function(_LoadStatusFailure value)? loadStatusFailure,
    TResult Function(_SuccessfulSavingData value)? successfulSavingData,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $MainStateCopyWith<$Res> {
  factory $MainStateCopyWith(MainState value, $Res Function(MainState) then) =
      _$MainStateCopyWithImpl<$Res>;
}

/// @nodoc
class _$MainStateCopyWithImpl<$Res> implements $MainStateCopyWith<$Res> {
  _$MainStateCopyWithImpl(this._value, this._then);

  final MainState _value;
  // ignore: unused_field
  final $Res Function(MainState) _then;
}

/// @nodoc
abstract class _$InitialCopyWith<$Res> {
  factory _$InitialCopyWith(_Initial value, $Res Function(_Initial) then) =
      __$InitialCopyWithImpl<$Res>;
}

/// @nodoc
class __$InitialCopyWithImpl<$Res> extends _$MainStateCopyWithImpl<$Res>
    implements _$InitialCopyWith<$Res> {
  __$InitialCopyWithImpl(_Initial _value, $Res Function(_Initial) _then)
      : super(_value, (v) => _then(v as _Initial));

  @override
  _Initial get _value => super._value as _Initial;
}

/// @nodoc

class _$_Initial implements _Initial {
  const _$_Initial();

  @override
  String toString() {
    return 'MainState.initial()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _Initial);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(List<Intent> intents, bool isEnabledToSave)
        showScreen,
    required TResult Function(IntentFailure intentFailure) saveChangesFailure,
    required TResult Function(IntentFailure intentFailure) loadFailure,
    required TResult Function() successfulDeletionIntent,
    required TResult Function() successfulDeletionExample,
    required TResult Function() intentAdded,
    required TResult Function(String status) showStatus,
    required TResult Function() loadStatusFailure,
    required TResult Function() successfulSavingData,
  }) {
    return initial();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(List<Intent> intents, bool isEnabledToSave)? showScreen,
    TResult Function(IntentFailure intentFailure)? saveChangesFailure,
    TResult Function(IntentFailure intentFailure)? loadFailure,
    TResult Function()? successfulDeletionIntent,
    TResult Function()? successfulDeletionExample,
    TResult Function()? intentAdded,
    TResult Function(String status)? showStatus,
    TResult Function()? loadStatusFailure,
    TResult Function()? successfulSavingData,
  }) {
    return initial?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(List<Intent> intents, bool isEnabledToSave)? showScreen,
    TResult Function(IntentFailure intentFailure)? saveChangesFailure,
    TResult Function(IntentFailure intentFailure)? loadFailure,
    TResult Function()? successfulDeletionIntent,
    TResult Function()? successfulDeletionExample,
    TResult Function()? intentAdded,
    TResult Function(String status)? showStatus,
    TResult Function()? loadStatusFailure,
    TResult Function()? successfulSavingData,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_Loading value) loading,
    required TResult Function(_ShowScreen value) showScreen,
    required TResult Function(_SaveChangesFailure value) saveChangesFailure,
    required TResult Function(_LoadFailure value) loadFailure,
    required TResult Function(_SuccessfulDeletionIntent value)
        successfulDeletionIntent,
    required TResult Function(_SuccessfulDeletionExample value)
        successfulDeletionExample,
    required TResult Function(_IntentAdded value) intentAdded,
    required TResult Function(_ShowStatus value) showStatus,
    required TResult Function(_LoadStatusFailure value) loadStatusFailure,
    required TResult Function(_SuccessfulSavingData value) successfulSavingData,
  }) {
    return initial(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_Loading value)? loading,
    TResult Function(_ShowScreen value)? showScreen,
    TResult Function(_SaveChangesFailure value)? saveChangesFailure,
    TResult Function(_LoadFailure value)? loadFailure,
    TResult Function(_SuccessfulDeletionIntent value)? successfulDeletionIntent,
    TResult Function(_SuccessfulDeletionExample value)?
        successfulDeletionExample,
    TResult Function(_IntentAdded value)? intentAdded,
    TResult Function(_ShowStatus value)? showStatus,
    TResult Function(_LoadStatusFailure value)? loadStatusFailure,
    TResult Function(_SuccessfulSavingData value)? successfulSavingData,
  }) {
    return initial?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_Loading value)? loading,
    TResult Function(_ShowScreen value)? showScreen,
    TResult Function(_SaveChangesFailure value)? saveChangesFailure,
    TResult Function(_LoadFailure value)? loadFailure,
    TResult Function(_SuccessfulDeletionIntent value)? successfulDeletionIntent,
    TResult Function(_SuccessfulDeletionExample value)?
        successfulDeletionExample,
    TResult Function(_IntentAdded value)? intentAdded,
    TResult Function(_ShowStatus value)? showStatus,
    TResult Function(_LoadStatusFailure value)? loadStatusFailure,
    TResult Function(_SuccessfulSavingData value)? successfulSavingData,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial(this);
    }
    return orElse();
  }
}

abstract class _Initial implements MainState {
  const factory _Initial() = _$_Initial;
}

/// @nodoc
abstract class _$LoadingCopyWith<$Res> {
  factory _$LoadingCopyWith(_Loading value, $Res Function(_Loading) then) =
      __$LoadingCopyWithImpl<$Res>;
}

/// @nodoc
class __$LoadingCopyWithImpl<$Res> extends _$MainStateCopyWithImpl<$Res>
    implements _$LoadingCopyWith<$Res> {
  __$LoadingCopyWithImpl(_Loading _value, $Res Function(_Loading) _then)
      : super(_value, (v) => _then(v as _Loading));

  @override
  _Loading get _value => super._value as _Loading;
}

/// @nodoc

class _$_Loading implements _Loading {
  const _$_Loading();

  @override
  String toString() {
    return 'MainState.loading()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _Loading);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(List<Intent> intents, bool isEnabledToSave)
        showScreen,
    required TResult Function(IntentFailure intentFailure) saveChangesFailure,
    required TResult Function(IntentFailure intentFailure) loadFailure,
    required TResult Function() successfulDeletionIntent,
    required TResult Function() successfulDeletionExample,
    required TResult Function() intentAdded,
    required TResult Function(String status) showStatus,
    required TResult Function() loadStatusFailure,
    required TResult Function() successfulSavingData,
  }) {
    return loading();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(List<Intent> intents, bool isEnabledToSave)? showScreen,
    TResult Function(IntentFailure intentFailure)? saveChangesFailure,
    TResult Function(IntentFailure intentFailure)? loadFailure,
    TResult Function()? successfulDeletionIntent,
    TResult Function()? successfulDeletionExample,
    TResult Function()? intentAdded,
    TResult Function(String status)? showStatus,
    TResult Function()? loadStatusFailure,
    TResult Function()? successfulSavingData,
  }) {
    return loading?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(List<Intent> intents, bool isEnabledToSave)? showScreen,
    TResult Function(IntentFailure intentFailure)? saveChangesFailure,
    TResult Function(IntentFailure intentFailure)? loadFailure,
    TResult Function()? successfulDeletionIntent,
    TResult Function()? successfulDeletionExample,
    TResult Function()? intentAdded,
    TResult Function(String status)? showStatus,
    TResult Function()? loadStatusFailure,
    TResult Function()? successfulSavingData,
    required TResult orElse(),
  }) {
    if (loading != null) {
      return loading();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_Loading value) loading,
    required TResult Function(_ShowScreen value) showScreen,
    required TResult Function(_SaveChangesFailure value) saveChangesFailure,
    required TResult Function(_LoadFailure value) loadFailure,
    required TResult Function(_SuccessfulDeletionIntent value)
        successfulDeletionIntent,
    required TResult Function(_SuccessfulDeletionExample value)
        successfulDeletionExample,
    required TResult Function(_IntentAdded value) intentAdded,
    required TResult Function(_ShowStatus value) showStatus,
    required TResult Function(_LoadStatusFailure value) loadStatusFailure,
    required TResult Function(_SuccessfulSavingData value) successfulSavingData,
  }) {
    return loading(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_Loading value)? loading,
    TResult Function(_ShowScreen value)? showScreen,
    TResult Function(_SaveChangesFailure value)? saveChangesFailure,
    TResult Function(_LoadFailure value)? loadFailure,
    TResult Function(_SuccessfulDeletionIntent value)? successfulDeletionIntent,
    TResult Function(_SuccessfulDeletionExample value)?
        successfulDeletionExample,
    TResult Function(_IntentAdded value)? intentAdded,
    TResult Function(_ShowStatus value)? showStatus,
    TResult Function(_LoadStatusFailure value)? loadStatusFailure,
    TResult Function(_SuccessfulSavingData value)? successfulSavingData,
  }) {
    return loading?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_Loading value)? loading,
    TResult Function(_ShowScreen value)? showScreen,
    TResult Function(_SaveChangesFailure value)? saveChangesFailure,
    TResult Function(_LoadFailure value)? loadFailure,
    TResult Function(_SuccessfulDeletionIntent value)? successfulDeletionIntent,
    TResult Function(_SuccessfulDeletionExample value)?
        successfulDeletionExample,
    TResult Function(_IntentAdded value)? intentAdded,
    TResult Function(_ShowStatus value)? showStatus,
    TResult Function(_LoadStatusFailure value)? loadStatusFailure,
    TResult Function(_SuccessfulSavingData value)? successfulSavingData,
    required TResult orElse(),
  }) {
    if (loading != null) {
      return loading(this);
    }
    return orElse();
  }
}

abstract class _Loading implements MainState {
  const factory _Loading() = _$_Loading;
}

/// @nodoc
abstract class _$ShowScreenCopyWith<$Res> {
  factory _$ShowScreenCopyWith(
          _ShowScreen value, $Res Function(_ShowScreen) then) =
      __$ShowScreenCopyWithImpl<$Res>;
  $Res call({List<Intent> intents, bool isEnabledToSave});
}

/// @nodoc
class __$ShowScreenCopyWithImpl<$Res> extends _$MainStateCopyWithImpl<$Res>
    implements _$ShowScreenCopyWith<$Res> {
  __$ShowScreenCopyWithImpl(
      _ShowScreen _value, $Res Function(_ShowScreen) _then)
      : super(_value, (v) => _then(v as _ShowScreen));

  @override
  _ShowScreen get _value => super._value as _ShowScreen;

  @override
  $Res call({
    Object? intents = freezed,
    Object? isEnabledToSave = freezed,
  }) {
    return _then(_ShowScreen(
      intents == freezed
          ? _value.intents
          : intents // ignore: cast_nullable_to_non_nullable
              as List<Intent>,
      isEnabledToSave == freezed
          ? _value.isEnabledToSave
          : isEnabledToSave // ignore: cast_nullable_to_non_nullable
              as bool,
    ));
  }
}

/// @nodoc

class _$_ShowScreen implements _ShowScreen {
  const _$_ShowScreen(this.intents, this.isEnabledToSave);

  @override
  final List<Intent> intents;
  @override
  final bool isEnabledToSave;

  @override
  String toString() {
    return 'MainState.showScreen(intents: $intents, isEnabledToSave: $isEnabledToSave)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _ShowScreen &&
            const DeepCollectionEquality().equals(other.intents, intents) &&
            const DeepCollectionEquality()
                .equals(other.isEnabledToSave, isEnabledToSave));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(intents),
      const DeepCollectionEquality().hash(isEnabledToSave));

  @JsonKey(ignore: true)
  @override
  _$ShowScreenCopyWith<_ShowScreen> get copyWith =>
      __$ShowScreenCopyWithImpl<_ShowScreen>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(List<Intent> intents, bool isEnabledToSave)
        showScreen,
    required TResult Function(IntentFailure intentFailure) saveChangesFailure,
    required TResult Function(IntentFailure intentFailure) loadFailure,
    required TResult Function() successfulDeletionIntent,
    required TResult Function() successfulDeletionExample,
    required TResult Function() intentAdded,
    required TResult Function(String status) showStatus,
    required TResult Function() loadStatusFailure,
    required TResult Function() successfulSavingData,
  }) {
    return showScreen(intents, isEnabledToSave);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(List<Intent> intents, bool isEnabledToSave)? showScreen,
    TResult Function(IntentFailure intentFailure)? saveChangesFailure,
    TResult Function(IntentFailure intentFailure)? loadFailure,
    TResult Function()? successfulDeletionIntent,
    TResult Function()? successfulDeletionExample,
    TResult Function()? intentAdded,
    TResult Function(String status)? showStatus,
    TResult Function()? loadStatusFailure,
    TResult Function()? successfulSavingData,
  }) {
    return showScreen?.call(intents, isEnabledToSave);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(List<Intent> intents, bool isEnabledToSave)? showScreen,
    TResult Function(IntentFailure intentFailure)? saveChangesFailure,
    TResult Function(IntentFailure intentFailure)? loadFailure,
    TResult Function()? successfulDeletionIntent,
    TResult Function()? successfulDeletionExample,
    TResult Function()? intentAdded,
    TResult Function(String status)? showStatus,
    TResult Function()? loadStatusFailure,
    TResult Function()? successfulSavingData,
    required TResult orElse(),
  }) {
    if (showScreen != null) {
      return showScreen(intents, isEnabledToSave);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_Loading value) loading,
    required TResult Function(_ShowScreen value) showScreen,
    required TResult Function(_SaveChangesFailure value) saveChangesFailure,
    required TResult Function(_LoadFailure value) loadFailure,
    required TResult Function(_SuccessfulDeletionIntent value)
        successfulDeletionIntent,
    required TResult Function(_SuccessfulDeletionExample value)
        successfulDeletionExample,
    required TResult Function(_IntentAdded value) intentAdded,
    required TResult Function(_ShowStatus value) showStatus,
    required TResult Function(_LoadStatusFailure value) loadStatusFailure,
    required TResult Function(_SuccessfulSavingData value) successfulSavingData,
  }) {
    return showScreen(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_Loading value)? loading,
    TResult Function(_ShowScreen value)? showScreen,
    TResult Function(_SaveChangesFailure value)? saveChangesFailure,
    TResult Function(_LoadFailure value)? loadFailure,
    TResult Function(_SuccessfulDeletionIntent value)? successfulDeletionIntent,
    TResult Function(_SuccessfulDeletionExample value)?
        successfulDeletionExample,
    TResult Function(_IntentAdded value)? intentAdded,
    TResult Function(_ShowStatus value)? showStatus,
    TResult Function(_LoadStatusFailure value)? loadStatusFailure,
    TResult Function(_SuccessfulSavingData value)? successfulSavingData,
  }) {
    return showScreen?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_Loading value)? loading,
    TResult Function(_ShowScreen value)? showScreen,
    TResult Function(_SaveChangesFailure value)? saveChangesFailure,
    TResult Function(_LoadFailure value)? loadFailure,
    TResult Function(_SuccessfulDeletionIntent value)? successfulDeletionIntent,
    TResult Function(_SuccessfulDeletionExample value)?
        successfulDeletionExample,
    TResult Function(_IntentAdded value)? intentAdded,
    TResult Function(_ShowStatus value)? showStatus,
    TResult Function(_LoadStatusFailure value)? loadStatusFailure,
    TResult Function(_SuccessfulSavingData value)? successfulSavingData,
    required TResult orElse(),
  }) {
    if (showScreen != null) {
      return showScreen(this);
    }
    return orElse();
  }
}

abstract class _ShowScreen implements MainState {
  const factory _ShowScreen(List<Intent> intents, bool isEnabledToSave) =
      _$_ShowScreen;

  List<Intent> get intents;
  bool get isEnabledToSave;
  @JsonKey(ignore: true)
  _$ShowScreenCopyWith<_ShowScreen> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$SaveChangesFailureCopyWith<$Res> {
  factory _$SaveChangesFailureCopyWith(
          _SaveChangesFailure value, $Res Function(_SaveChangesFailure) then) =
      __$SaveChangesFailureCopyWithImpl<$Res>;
  $Res call({IntentFailure intentFailure});

  $IntentFailureCopyWith<$Res> get intentFailure;
}

/// @nodoc
class __$SaveChangesFailureCopyWithImpl<$Res>
    extends _$MainStateCopyWithImpl<$Res>
    implements _$SaveChangesFailureCopyWith<$Res> {
  __$SaveChangesFailureCopyWithImpl(
      _SaveChangesFailure _value, $Res Function(_SaveChangesFailure) _then)
      : super(_value, (v) => _then(v as _SaveChangesFailure));

  @override
  _SaveChangesFailure get _value => super._value as _SaveChangesFailure;

  @override
  $Res call({
    Object? intentFailure = freezed,
  }) {
    return _then(_SaveChangesFailure(
      intentFailure == freezed
          ? _value.intentFailure
          : intentFailure // ignore: cast_nullable_to_non_nullable
              as IntentFailure,
    ));
  }

  @override
  $IntentFailureCopyWith<$Res> get intentFailure {
    return $IntentFailureCopyWith<$Res>(_value.intentFailure, (value) {
      return _then(_value.copyWith(intentFailure: value));
    });
  }
}

/// @nodoc

class _$_SaveChangesFailure implements _SaveChangesFailure {
  const _$_SaveChangesFailure(this.intentFailure);

  @override
  final IntentFailure intentFailure;

  @override
  String toString() {
    return 'MainState.saveChangesFailure(intentFailure: $intentFailure)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _SaveChangesFailure &&
            const DeepCollectionEquality()
                .equals(other.intentFailure, intentFailure));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType, const DeepCollectionEquality().hash(intentFailure));

  @JsonKey(ignore: true)
  @override
  _$SaveChangesFailureCopyWith<_SaveChangesFailure> get copyWith =>
      __$SaveChangesFailureCopyWithImpl<_SaveChangesFailure>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(List<Intent> intents, bool isEnabledToSave)
        showScreen,
    required TResult Function(IntentFailure intentFailure) saveChangesFailure,
    required TResult Function(IntentFailure intentFailure) loadFailure,
    required TResult Function() successfulDeletionIntent,
    required TResult Function() successfulDeletionExample,
    required TResult Function() intentAdded,
    required TResult Function(String status) showStatus,
    required TResult Function() loadStatusFailure,
    required TResult Function() successfulSavingData,
  }) {
    return saveChangesFailure(intentFailure);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(List<Intent> intents, bool isEnabledToSave)? showScreen,
    TResult Function(IntentFailure intentFailure)? saveChangesFailure,
    TResult Function(IntentFailure intentFailure)? loadFailure,
    TResult Function()? successfulDeletionIntent,
    TResult Function()? successfulDeletionExample,
    TResult Function()? intentAdded,
    TResult Function(String status)? showStatus,
    TResult Function()? loadStatusFailure,
    TResult Function()? successfulSavingData,
  }) {
    return saveChangesFailure?.call(intentFailure);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(List<Intent> intents, bool isEnabledToSave)? showScreen,
    TResult Function(IntentFailure intentFailure)? saveChangesFailure,
    TResult Function(IntentFailure intentFailure)? loadFailure,
    TResult Function()? successfulDeletionIntent,
    TResult Function()? successfulDeletionExample,
    TResult Function()? intentAdded,
    TResult Function(String status)? showStatus,
    TResult Function()? loadStatusFailure,
    TResult Function()? successfulSavingData,
    required TResult orElse(),
  }) {
    if (saveChangesFailure != null) {
      return saveChangesFailure(intentFailure);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_Loading value) loading,
    required TResult Function(_ShowScreen value) showScreen,
    required TResult Function(_SaveChangesFailure value) saveChangesFailure,
    required TResult Function(_LoadFailure value) loadFailure,
    required TResult Function(_SuccessfulDeletionIntent value)
        successfulDeletionIntent,
    required TResult Function(_SuccessfulDeletionExample value)
        successfulDeletionExample,
    required TResult Function(_IntentAdded value) intentAdded,
    required TResult Function(_ShowStatus value) showStatus,
    required TResult Function(_LoadStatusFailure value) loadStatusFailure,
    required TResult Function(_SuccessfulSavingData value) successfulSavingData,
  }) {
    return saveChangesFailure(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_Loading value)? loading,
    TResult Function(_ShowScreen value)? showScreen,
    TResult Function(_SaveChangesFailure value)? saveChangesFailure,
    TResult Function(_LoadFailure value)? loadFailure,
    TResult Function(_SuccessfulDeletionIntent value)? successfulDeletionIntent,
    TResult Function(_SuccessfulDeletionExample value)?
        successfulDeletionExample,
    TResult Function(_IntentAdded value)? intentAdded,
    TResult Function(_ShowStatus value)? showStatus,
    TResult Function(_LoadStatusFailure value)? loadStatusFailure,
    TResult Function(_SuccessfulSavingData value)? successfulSavingData,
  }) {
    return saveChangesFailure?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_Loading value)? loading,
    TResult Function(_ShowScreen value)? showScreen,
    TResult Function(_SaveChangesFailure value)? saveChangesFailure,
    TResult Function(_LoadFailure value)? loadFailure,
    TResult Function(_SuccessfulDeletionIntent value)? successfulDeletionIntent,
    TResult Function(_SuccessfulDeletionExample value)?
        successfulDeletionExample,
    TResult Function(_IntentAdded value)? intentAdded,
    TResult Function(_ShowStatus value)? showStatus,
    TResult Function(_LoadStatusFailure value)? loadStatusFailure,
    TResult Function(_SuccessfulSavingData value)? successfulSavingData,
    required TResult orElse(),
  }) {
    if (saveChangesFailure != null) {
      return saveChangesFailure(this);
    }
    return orElse();
  }
}

abstract class _SaveChangesFailure implements MainState {
  const factory _SaveChangesFailure(IntentFailure intentFailure) =
      _$_SaveChangesFailure;

  IntentFailure get intentFailure;
  @JsonKey(ignore: true)
  _$SaveChangesFailureCopyWith<_SaveChangesFailure> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$LoadFailureCopyWith<$Res> {
  factory _$LoadFailureCopyWith(
          _LoadFailure value, $Res Function(_LoadFailure) then) =
      __$LoadFailureCopyWithImpl<$Res>;
  $Res call({IntentFailure intentFailure});

  $IntentFailureCopyWith<$Res> get intentFailure;
}

/// @nodoc
class __$LoadFailureCopyWithImpl<$Res> extends _$MainStateCopyWithImpl<$Res>
    implements _$LoadFailureCopyWith<$Res> {
  __$LoadFailureCopyWithImpl(
      _LoadFailure _value, $Res Function(_LoadFailure) _then)
      : super(_value, (v) => _then(v as _LoadFailure));

  @override
  _LoadFailure get _value => super._value as _LoadFailure;

  @override
  $Res call({
    Object? intentFailure = freezed,
  }) {
    return _then(_LoadFailure(
      intentFailure == freezed
          ? _value.intentFailure
          : intentFailure // ignore: cast_nullable_to_non_nullable
              as IntentFailure,
    ));
  }

  @override
  $IntentFailureCopyWith<$Res> get intentFailure {
    return $IntentFailureCopyWith<$Res>(_value.intentFailure, (value) {
      return _then(_value.copyWith(intentFailure: value));
    });
  }
}

/// @nodoc

class _$_LoadFailure implements _LoadFailure {
  const _$_LoadFailure(this.intentFailure);

  @override
  final IntentFailure intentFailure;

  @override
  String toString() {
    return 'MainState.loadFailure(intentFailure: $intentFailure)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _LoadFailure &&
            const DeepCollectionEquality()
                .equals(other.intentFailure, intentFailure));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType, const DeepCollectionEquality().hash(intentFailure));

  @JsonKey(ignore: true)
  @override
  _$LoadFailureCopyWith<_LoadFailure> get copyWith =>
      __$LoadFailureCopyWithImpl<_LoadFailure>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(List<Intent> intents, bool isEnabledToSave)
        showScreen,
    required TResult Function(IntentFailure intentFailure) saveChangesFailure,
    required TResult Function(IntentFailure intentFailure) loadFailure,
    required TResult Function() successfulDeletionIntent,
    required TResult Function() successfulDeletionExample,
    required TResult Function() intentAdded,
    required TResult Function(String status) showStatus,
    required TResult Function() loadStatusFailure,
    required TResult Function() successfulSavingData,
  }) {
    return loadFailure(intentFailure);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(List<Intent> intents, bool isEnabledToSave)? showScreen,
    TResult Function(IntentFailure intentFailure)? saveChangesFailure,
    TResult Function(IntentFailure intentFailure)? loadFailure,
    TResult Function()? successfulDeletionIntent,
    TResult Function()? successfulDeletionExample,
    TResult Function()? intentAdded,
    TResult Function(String status)? showStatus,
    TResult Function()? loadStatusFailure,
    TResult Function()? successfulSavingData,
  }) {
    return loadFailure?.call(intentFailure);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(List<Intent> intents, bool isEnabledToSave)? showScreen,
    TResult Function(IntentFailure intentFailure)? saveChangesFailure,
    TResult Function(IntentFailure intentFailure)? loadFailure,
    TResult Function()? successfulDeletionIntent,
    TResult Function()? successfulDeletionExample,
    TResult Function()? intentAdded,
    TResult Function(String status)? showStatus,
    TResult Function()? loadStatusFailure,
    TResult Function()? successfulSavingData,
    required TResult orElse(),
  }) {
    if (loadFailure != null) {
      return loadFailure(intentFailure);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_Loading value) loading,
    required TResult Function(_ShowScreen value) showScreen,
    required TResult Function(_SaveChangesFailure value) saveChangesFailure,
    required TResult Function(_LoadFailure value) loadFailure,
    required TResult Function(_SuccessfulDeletionIntent value)
        successfulDeletionIntent,
    required TResult Function(_SuccessfulDeletionExample value)
        successfulDeletionExample,
    required TResult Function(_IntentAdded value) intentAdded,
    required TResult Function(_ShowStatus value) showStatus,
    required TResult Function(_LoadStatusFailure value) loadStatusFailure,
    required TResult Function(_SuccessfulSavingData value) successfulSavingData,
  }) {
    return loadFailure(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_Loading value)? loading,
    TResult Function(_ShowScreen value)? showScreen,
    TResult Function(_SaveChangesFailure value)? saveChangesFailure,
    TResult Function(_LoadFailure value)? loadFailure,
    TResult Function(_SuccessfulDeletionIntent value)? successfulDeletionIntent,
    TResult Function(_SuccessfulDeletionExample value)?
        successfulDeletionExample,
    TResult Function(_IntentAdded value)? intentAdded,
    TResult Function(_ShowStatus value)? showStatus,
    TResult Function(_LoadStatusFailure value)? loadStatusFailure,
    TResult Function(_SuccessfulSavingData value)? successfulSavingData,
  }) {
    return loadFailure?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_Loading value)? loading,
    TResult Function(_ShowScreen value)? showScreen,
    TResult Function(_SaveChangesFailure value)? saveChangesFailure,
    TResult Function(_LoadFailure value)? loadFailure,
    TResult Function(_SuccessfulDeletionIntent value)? successfulDeletionIntent,
    TResult Function(_SuccessfulDeletionExample value)?
        successfulDeletionExample,
    TResult Function(_IntentAdded value)? intentAdded,
    TResult Function(_ShowStatus value)? showStatus,
    TResult Function(_LoadStatusFailure value)? loadStatusFailure,
    TResult Function(_SuccessfulSavingData value)? successfulSavingData,
    required TResult orElse(),
  }) {
    if (loadFailure != null) {
      return loadFailure(this);
    }
    return orElse();
  }
}

abstract class _LoadFailure implements MainState {
  const factory _LoadFailure(IntentFailure intentFailure) = _$_LoadFailure;

  IntentFailure get intentFailure;
  @JsonKey(ignore: true)
  _$LoadFailureCopyWith<_LoadFailure> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$SuccessfulDeletionIntentCopyWith<$Res> {
  factory _$SuccessfulDeletionIntentCopyWith(_SuccessfulDeletionIntent value,
          $Res Function(_SuccessfulDeletionIntent) then) =
      __$SuccessfulDeletionIntentCopyWithImpl<$Res>;
}

/// @nodoc
class __$SuccessfulDeletionIntentCopyWithImpl<$Res>
    extends _$MainStateCopyWithImpl<$Res>
    implements _$SuccessfulDeletionIntentCopyWith<$Res> {
  __$SuccessfulDeletionIntentCopyWithImpl(_SuccessfulDeletionIntent _value,
      $Res Function(_SuccessfulDeletionIntent) _then)
      : super(_value, (v) => _then(v as _SuccessfulDeletionIntent));

  @override
  _SuccessfulDeletionIntent get _value =>
      super._value as _SuccessfulDeletionIntent;
}

/// @nodoc

class _$_SuccessfulDeletionIntent implements _SuccessfulDeletionIntent {
  const _$_SuccessfulDeletionIntent();

  @override
  String toString() {
    return 'MainState.successfulDeletionIntent()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _SuccessfulDeletionIntent);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(List<Intent> intents, bool isEnabledToSave)
        showScreen,
    required TResult Function(IntentFailure intentFailure) saveChangesFailure,
    required TResult Function(IntentFailure intentFailure) loadFailure,
    required TResult Function() successfulDeletionIntent,
    required TResult Function() successfulDeletionExample,
    required TResult Function() intentAdded,
    required TResult Function(String status) showStatus,
    required TResult Function() loadStatusFailure,
    required TResult Function() successfulSavingData,
  }) {
    return successfulDeletionIntent();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(List<Intent> intents, bool isEnabledToSave)? showScreen,
    TResult Function(IntentFailure intentFailure)? saveChangesFailure,
    TResult Function(IntentFailure intentFailure)? loadFailure,
    TResult Function()? successfulDeletionIntent,
    TResult Function()? successfulDeletionExample,
    TResult Function()? intentAdded,
    TResult Function(String status)? showStatus,
    TResult Function()? loadStatusFailure,
    TResult Function()? successfulSavingData,
  }) {
    return successfulDeletionIntent?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(List<Intent> intents, bool isEnabledToSave)? showScreen,
    TResult Function(IntentFailure intentFailure)? saveChangesFailure,
    TResult Function(IntentFailure intentFailure)? loadFailure,
    TResult Function()? successfulDeletionIntent,
    TResult Function()? successfulDeletionExample,
    TResult Function()? intentAdded,
    TResult Function(String status)? showStatus,
    TResult Function()? loadStatusFailure,
    TResult Function()? successfulSavingData,
    required TResult orElse(),
  }) {
    if (successfulDeletionIntent != null) {
      return successfulDeletionIntent();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_Loading value) loading,
    required TResult Function(_ShowScreen value) showScreen,
    required TResult Function(_SaveChangesFailure value) saveChangesFailure,
    required TResult Function(_LoadFailure value) loadFailure,
    required TResult Function(_SuccessfulDeletionIntent value)
        successfulDeletionIntent,
    required TResult Function(_SuccessfulDeletionExample value)
        successfulDeletionExample,
    required TResult Function(_IntentAdded value) intentAdded,
    required TResult Function(_ShowStatus value) showStatus,
    required TResult Function(_LoadStatusFailure value) loadStatusFailure,
    required TResult Function(_SuccessfulSavingData value) successfulSavingData,
  }) {
    return successfulDeletionIntent(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_Loading value)? loading,
    TResult Function(_ShowScreen value)? showScreen,
    TResult Function(_SaveChangesFailure value)? saveChangesFailure,
    TResult Function(_LoadFailure value)? loadFailure,
    TResult Function(_SuccessfulDeletionIntent value)? successfulDeletionIntent,
    TResult Function(_SuccessfulDeletionExample value)?
        successfulDeletionExample,
    TResult Function(_IntentAdded value)? intentAdded,
    TResult Function(_ShowStatus value)? showStatus,
    TResult Function(_LoadStatusFailure value)? loadStatusFailure,
    TResult Function(_SuccessfulSavingData value)? successfulSavingData,
  }) {
    return successfulDeletionIntent?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_Loading value)? loading,
    TResult Function(_ShowScreen value)? showScreen,
    TResult Function(_SaveChangesFailure value)? saveChangesFailure,
    TResult Function(_LoadFailure value)? loadFailure,
    TResult Function(_SuccessfulDeletionIntent value)? successfulDeletionIntent,
    TResult Function(_SuccessfulDeletionExample value)?
        successfulDeletionExample,
    TResult Function(_IntentAdded value)? intentAdded,
    TResult Function(_ShowStatus value)? showStatus,
    TResult Function(_LoadStatusFailure value)? loadStatusFailure,
    TResult Function(_SuccessfulSavingData value)? successfulSavingData,
    required TResult orElse(),
  }) {
    if (successfulDeletionIntent != null) {
      return successfulDeletionIntent(this);
    }
    return orElse();
  }
}

abstract class _SuccessfulDeletionIntent implements MainState {
  const factory _SuccessfulDeletionIntent() = _$_SuccessfulDeletionIntent;
}

/// @nodoc
abstract class _$SuccessfulDeletionExampleCopyWith<$Res> {
  factory _$SuccessfulDeletionExampleCopyWith(_SuccessfulDeletionExample value,
          $Res Function(_SuccessfulDeletionExample) then) =
      __$SuccessfulDeletionExampleCopyWithImpl<$Res>;
}

/// @nodoc
class __$SuccessfulDeletionExampleCopyWithImpl<$Res>
    extends _$MainStateCopyWithImpl<$Res>
    implements _$SuccessfulDeletionExampleCopyWith<$Res> {
  __$SuccessfulDeletionExampleCopyWithImpl(_SuccessfulDeletionExample _value,
      $Res Function(_SuccessfulDeletionExample) _then)
      : super(_value, (v) => _then(v as _SuccessfulDeletionExample));

  @override
  _SuccessfulDeletionExample get _value =>
      super._value as _SuccessfulDeletionExample;
}

/// @nodoc

class _$_SuccessfulDeletionExample implements _SuccessfulDeletionExample {
  const _$_SuccessfulDeletionExample();

  @override
  String toString() {
    return 'MainState.successfulDeletionExample()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _SuccessfulDeletionExample);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(List<Intent> intents, bool isEnabledToSave)
        showScreen,
    required TResult Function(IntentFailure intentFailure) saveChangesFailure,
    required TResult Function(IntentFailure intentFailure) loadFailure,
    required TResult Function() successfulDeletionIntent,
    required TResult Function() successfulDeletionExample,
    required TResult Function() intentAdded,
    required TResult Function(String status) showStatus,
    required TResult Function() loadStatusFailure,
    required TResult Function() successfulSavingData,
  }) {
    return successfulDeletionExample();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(List<Intent> intents, bool isEnabledToSave)? showScreen,
    TResult Function(IntentFailure intentFailure)? saveChangesFailure,
    TResult Function(IntentFailure intentFailure)? loadFailure,
    TResult Function()? successfulDeletionIntent,
    TResult Function()? successfulDeletionExample,
    TResult Function()? intentAdded,
    TResult Function(String status)? showStatus,
    TResult Function()? loadStatusFailure,
    TResult Function()? successfulSavingData,
  }) {
    return successfulDeletionExample?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(List<Intent> intents, bool isEnabledToSave)? showScreen,
    TResult Function(IntentFailure intentFailure)? saveChangesFailure,
    TResult Function(IntentFailure intentFailure)? loadFailure,
    TResult Function()? successfulDeletionIntent,
    TResult Function()? successfulDeletionExample,
    TResult Function()? intentAdded,
    TResult Function(String status)? showStatus,
    TResult Function()? loadStatusFailure,
    TResult Function()? successfulSavingData,
    required TResult orElse(),
  }) {
    if (successfulDeletionExample != null) {
      return successfulDeletionExample();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_Loading value) loading,
    required TResult Function(_ShowScreen value) showScreen,
    required TResult Function(_SaveChangesFailure value) saveChangesFailure,
    required TResult Function(_LoadFailure value) loadFailure,
    required TResult Function(_SuccessfulDeletionIntent value)
        successfulDeletionIntent,
    required TResult Function(_SuccessfulDeletionExample value)
        successfulDeletionExample,
    required TResult Function(_IntentAdded value) intentAdded,
    required TResult Function(_ShowStatus value) showStatus,
    required TResult Function(_LoadStatusFailure value) loadStatusFailure,
    required TResult Function(_SuccessfulSavingData value) successfulSavingData,
  }) {
    return successfulDeletionExample(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_Loading value)? loading,
    TResult Function(_ShowScreen value)? showScreen,
    TResult Function(_SaveChangesFailure value)? saveChangesFailure,
    TResult Function(_LoadFailure value)? loadFailure,
    TResult Function(_SuccessfulDeletionIntent value)? successfulDeletionIntent,
    TResult Function(_SuccessfulDeletionExample value)?
        successfulDeletionExample,
    TResult Function(_IntentAdded value)? intentAdded,
    TResult Function(_ShowStatus value)? showStatus,
    TResult Function(_LoadStatusFailure value)? loadStatusFailure,
    TResult Function(_SuccessfulSavingData value)? successfulSavingData,
  }) {
    return successfulDeletionExample?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_Loading value)? loading,
    TResult Function(_ShowScreen value)? showScreen,
    TResult Function(_SaveChangesFailure value)? saveChangesFailure,
    TResult Function(_LoadFailure value)? loadFailure,
    TResult Function(_SuccessfulDeletionIntent value)? successfulDeletionIntent,
    TResult Function(_SuccessfulDeletionExample value)?
        successfulDeletionExample,
    TResult Function(_IntentAdded value)? intentAdded,
    TResult Function(_ShowStatus value)? showStatus,
    TResult Function(_LoadStatusFailure value)? loadStatusFailure,
    TResult Function(_SuccessfulSavingData value)? successfulSavingData,
    required TResult orElse(),
  }) {
    if (successfulDeletionExample != null) {
      return successfulDeletionExample(this);
    }
    return orElse();
  }
}

abstract class _SuccessfulDeletionExample implements MainState {
  const factory _SuccessfulDeletionExample() = _$_SuccessfulDeletionExample;
}

/// @nodoc
abstract class _$IntentAddedCopyWith<$Res> {
  factory _$IntentAddedCopyWith(
          _IntentAdded value, $Res Function(_IntentAdded) then) =
      __$IntentAddedCopyWithImpl<$Res>;
}

/// @nodoc
class __$IntentAddedCopyWithImpl<$Res> extends _$MainStateCopyWithImpl<$Res>
    implements _$IntentAddedCopyWith<$Res> {
  __$IntentAddedCopyWithImpl(
      _IntentAdded _value, $Res Function(_IntentAdded) _then)
      : super(_value, (v) => _then(v as _IntentAdded));

  @override
  _IntentAdded get _value => super._value as _IntentAdded;
}

/// @nodoc

class _$_IntentAdded implements _IntentAdded {
  const _$_IntentAdded();

  @override
  String toString() {
    return 'MainState.intentAdded()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _IntentAdded);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(List<Intent> intents, bool isEnabledToSave)
        showScreen,
    required TResult Function(IntentFailure intentFailure) saveChangesFailure,
    required TResult Function(IntentFailure intentFailure) loadFailure,
    required TResult Function() successfulDeletionIntent,
    required TResult Function() successfulDeletionExample,
    required TResult Function() intentAdded,
    required TResult Function(String status) showStatus,
    required TResult Function() loadStatusFailure,
    required TResult Function() successfulSavingData,
  }) {
    return intentAdded();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(List<Intent> intents, bool isEnabledToSave)? showScreen,
    TResult Function(IntentFailure intentFailure)? saveChangesFailure,
    TResult Function(IntentFailure intentFailure)? loadFailure,
    TResult Function()? successfulDeletionIntent,
    TResult Function()? successfulDeletionExample,
    TResult Function()? intentAdded,
    TResult Function(String status)? showStatus,
    TResult Function()? loadStatusFailure,
    TResult Function()? successfulSavingData,
  }) {
    return intentAdded?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(List<Intent> intents, bool isEnabledToSave)? showScreen,
    TResult Function(IntentFailure intentFailure)? saveChangesFailure,
    TResult Function(IntentFailure intentFailure)? loadFailure,
    TResult Function()? successfulDeletionIntent,
    TResult Function()? successfulDeletionExample,
    TResult Function()? intentAdded,
    TResult Function(String status)? showStatus,
    TResult Function()? loadStatusFailure,
    TResult Function()? successfulSavingData,
    required TResult orElse(),
  }) {
    if (intentAdded != null) {
      return intentAdded();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_Loading value) loading,
    required TResult Function(_ShowScreen value) showScreen,
    required TResult Function(_SaveChangesFailure value) saveChangesFailure,
    required TResult Function(_LoadFailure value) loadFailure,
    required TResult Function(_SuccessfulDeletionIntent value)
        successfulDeletionIntent,
    required TResult Function(_SuccessfulDeletionExample value)
        successfulDeletionExample,
    required TResult Function(_IntentAdded value) intentAdded,
    required TResult Function(_ShowStatus value) showStatus,
    required TResult Function(_LoadStatusFailure value) loadStatusFailure,
    required TResult Function(_SuccessfulSavingData value) successfulSavingData,
  }) {
    return intentAdded(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_Loading value)? loading,
    TResult Function(_ShowScreen value)? showScreen,
    TResult Function(_SaveChangesFailure value)? saveChangesFailure,
    TResult Function(_LoadFailure value)? loadFailure,
    TResult Function(_SuccessfulDeletionIntent value)? successfulDeletionIntent,
    TResult Function(_SuccessfulDeletionExample value)?
        successfulDeletionExample,
    TResult Function(_IntentAdded value)? intentAdded,
    TResult Function(_ShowStatus value)? showStatus,
    TResult Function(_LoadStatusFailure value)? loadStatusFailure,
    TResult Function(_SuccessfulSavingData value)? successfulSavingData,
  }) {
    return intentAdded?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_Loading value)? loading,
    TResult Function(_ShowScreen value)? showScreen,
    TResult Function(_SaveChangesFailure value)? saveChangesFailure,
    TResult Function(_LoadFailure value)? loadFailure,
    TResult Function(_SuccessfulDeletionIntent value)? successfulDeletionIntent,
    TResult Function(_SuccessfulDeletionExample value)?
        successfulDeletionExample,
    TResult Function(_IntentAdded value)? intentAdded,
    TResult Function(_ShowStatus value)? showStatus,
    TResult Function(_LoadStatusFailure value)? loadStatusFailure,
    TResult Function(_SuccessfulSavingData value)? successfulSavingData,
    required TResult orElse(),
  }) {
    if (intentAdded != null) {
      return intentAdded(this);
    }
    return orElse();
  }
}

abstract class _IntentAdded implements MainState {
  const factory _IntentAdded() = _$_IntentAdded;
}

/// @nodoc
abstract class _$ShowStatusCopyWith<$Res> {
  factory _$ShowStatusCopyWith(
          _ShowStatus value, $Res Function(_ShowStatus) then) =
      __$ShowStatusCopyWithImpl<$Res>;
  $Res call({String status});
}

/// @nodoc
class __$ShowStatusCopyWithImpl<$Res> extends _$MainStateCopyWithImpl<$Res>
    implements _$ShowStatusCopyWith<$Res> {
  __$ShowStatusCopyWithImpl(
      _ShowStatus _value, $Res Function(_ShowStatus) _then)
      : super(_value, (v) => _then(v as _ShowStatus));

  @override
  _ShowStatus get _value => super._value as _ShowStatus;

  @override
  $Res call({
    Object? status = freezed,
  }) {
    return _then(_ShowStatus(
      status == freezed
          ? _value.status
          : status // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$_ShowStatus implements _ShowStatus {
  const _$_ShowStatus(this.status);

  @override
  final String status;

  @override
  String toString() {
    return 'MainState.showStatus(status: $status)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _ShowStatus &&
            const DeepCollectionEquality().equals(other.status, status));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(status));

  @JsonKey(ignore: true)
  @override
  _$ShowStatusCopyWith<_ShowStatus> get copyWith =>
      __$ShowStatusCopyWithImpl<_ShowStatus>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(List<Intent> intents, bool isEnabledToSave)
        showScreen,
    required TResult Function(IntentFailure intentFailure) saveChangesFailure,
    required TResult Function(IntentFailure intentFailure) loadFailure,
    required TResult Function() successfulDeletionIntent,
    required TResult Function() successfulDeletionExample,
    required TResult Function() intentAdded,
    required TResult Function(String status) showStatus,
    required TResult Function() loadStatusFailure,
    required TResult Function() successfulSavingData,
  }) {
    return showStatus(status);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(List<Intent> intents, bool isEnabledToSave)? showScreen,
    TResult Function(IntentFailure intentFailure)? saveChangesFailure,
    TResult Function(IntentFailure intentFailure)? loadFailure,
    TResult Function()? successfulDeletionIntent,
    TResult Function()? successfulDeletionExample,
    TResult Function()? intentAdded,
    TResult Function(String status)? showStatus,
    TResult Function()? loadStatusFailure,
    TResult Function()? successfulSavingData,
  }) {
    return showStatus?.call(status);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(List<Intent> intents, bool isEnabledToSave)? showScreen,
    TResult Function(IntentFailure intentFailure)? saveChangesFailure,
    TResult Function(IntentFailure intentFailure)? loadFailure,
    TResult Function()? successfulDeletionIntent,
    TResult Function()? successfulDeletionExample,
    TResult Function()? intentAdded,
    TResult Function(String status)? showStatus,
    TResult Function()? loadStatusFailure,
    TResult Function()? successfulSavingData,
    required TResult orElse(),
  }) {
    if (showStatus != null) {
      return showStatus(status);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_Loading value) loading,
    required TResult Function(_ShowScreen value) showScreen,
    required TResult Function(_SaveChangesFailure value) saveChangesFailure,
    required TResult Function(_LoadFailure value) loadFailure,
    required TResult Function(_SuccessfulDeletionIntent value)
        successfulDeletionIntent,
    required TResult Function(_SuccessfulDeletionExample value)
        successfulDeletionExample,
    required TResult Function(_IntentAdded value) intentAdded,
    required TResult Function(_ShowStatus value) showStatus,
    required TResult Function(_LoadStatusFailure value) loadStatusFailure,
    required TResult Function(_SuccessfulSavingData value) successfulSavingData,
  }) {
    return showStatus(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_Loading value)? loading,
    TResult Function(_ShowScreen value)? showScreen,
    TResult Function(_SaveChangesFailure value)? saveChangesFailure,
    TResult Function(_LoadFailure value)? loadFailure,
    TResult Function(_SuccessfulDeletionIntent value)? successfulDeletionIntent,
    TResult Function(_SuccessfulDeletionExample value)?
        successfulDeletionExample,
    TResult Function(_IntentAdded value)? intentAdded,
    TResult Function(_ShowStatus value)? showStatus,
    TResult Function(_LoadStatusFailure value)? loadStatusFailure,
    TResult Function(_SuccessfulSavingData value)? successfulSavingData,
  }) {
    return showStatus?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_Loading value)? loading,
    TResult Function(_ShowScreen value)? showScreen,
    TResult Function(_SaveChangesFailure value)? saveChangesFailure,
    TResult Function(_LoadFailure value)? loadFailure,
    TResult Function(_SuccessfulDeletionIntent value)? successfulDeletionIntent,
    TResult Function(_SuccessfulDeletionExample value)?
        successfulDeletionExample,
    TResult Function(_IntentAdded value)? intentAdded,
    TResult Function(_ShowStatus value)? showStatus,
    TResult Function(_LoadStatusFailure value)? loadStatusFailure,
    TResult Function(_SuccessfulSavingData value)? successfulSavingData,
    required TResult orElse(),
  }) {
    if (showStatus != null) {
      return showStatus(this);
    }
    return orElse();
  }
}

abstract class _ShowStatus implements MainState {
  const factory _ShowStatus(String status) = _$_ShowStatus;

  String get status;
  @JsonKey(ignore: true)
  _$ShowStatusCopyWith<_ShowStatus> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$LoadStatusFailureCopyWith<$Res> {
  factory _$LoadStatusFailureCopyWith(
          _LoadStatusFailure value, $Res Function(_LoadStatusFailure) then) =
      __$LoadStatusFailureCopyWithImpl<$Res>;
}

/// @nodoc
class __$LoadStatusFailureCopyWithImpl<$Res>
    extends _$MainStateCopyWithImpl<$Res>
    implements _$LoadStatusFailureCopyWith<$Res> {
  __$LoadStatusFailureCopyWithImpl(
      _LoadStatusFailure _value, $Res Function(_LoadStatusFailure) _then)
      : super(_value, (v) => _then(v as _LoadStatusFailure));

  @override
  _LoadStatusFailure get _value => super._value as _LoadStatusFailure;
}

/// @nodoc

class _$_LoadStatusFailure implements _LoadStatusFailure {
  const _$_LoadStatusFailure();

  @override
  String toString() {
    return 'MainState.loadStatusFailure()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _LoadStatusFailure);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(List<Intent> intents, bool isEnabledToSave)
        showScreen,
    required TResult Function(IntentFailure intentFailure) saveChangesFailure,
    required TResult Function(IntentFailure intentFailure) loadFailure,
    required TResult Function() successfulDeletionIntent,
    required TResult Function() successfulDeletionExample,
    required TResult Function() intentAdded,
    required TResult Function(String status) showStatus,
    required TResult Function() loadStatusFailure,
    required TResult Function() successfulSavingData,
  }) {
    return loadStatusFailure();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(List<Intent> intents, bool isEnabledToSave)? showScreen,
    TResult Function(IntentFailure intentFailure)? saveChangesFailure,
    TResult Function(IntentFailure intentFailure)? loadFailure,
    TResult Function()? successfulDeletionIntent,
    TResult Function()? successfulDeletionExample,
    TResult Function()? intentAdded,
    TResult Function(String status)? showStatus,
    TResult Function()? loadStatusFailure,
    TResult Function()? successfulSavingData,
  }) {
    return loadStatusFailure?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(List<Intent> intents, bool isEnabledToSave)? showScreen,
    TResult Function(IntentFailure intentFailure)? saveChangesFailure,
    TResult Function(IntentFailure intentFailure)? loadFailure,
    TResult Function()? successfulDeletionIntent,
    TResult Function()? successfulDeletionExample,
    TResult Function()? intentAdded,
    TResult Function(String status)? showStatus,
    TResult Function()? loadStatusFailure,
    TResult Function()? successfulSavingData,
    required TResult orElse(),
  }) {
    if (loadStatusFailure != null) {
      return loadStatusFailure();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_Loading value) loading,
    required TResult Function(_ShowScreen value) showScreen,
    required TResult Function(_SaveChangesFailure value) saveChangesFailure,
    required TResult Function(_LoadFailure value) loadFailure,
    required TResult Function(_SuccessfulDeletionIntent value)
        successfulDeletionIntent,
    required TResult Function(_SuccessfulDeletionExample value)
        successfulDeletionExample,
    required TResult Function(_IntentAdded value) intentAdded,
    required TResult Function(_ShowStatus value) showStatus,
    required TResult Function(_LoadStatusFailure value) loadStatusFailure,
    required TResult Function(_SuccessfulSavingData value) successfulSavingData,
  }) {
    return loadStatusFailure(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_Loading value)? loading,
    TResult Function(_ShowScreen value)? showScreen,
    TResult Function(_SaveChangesFailure value)? saveChangesFailure,
    TResult Function(_LoadFailure value)? loadFailure,
    TResult Function(_SuccessfulDeletionIntent value)? successfulDeletionIntent,
    TResult Function(_SuccessfulDeletionExample value)?
        successfulDeletionExample,
    TResult Function(_IntentAdded value)? intentAdded,
    TResult Function(_ShowStatus value)? showStatus,
    TResult Function(_LoadStatusFailure value)? loadStatusFailure,
    TResult Function(_SuccessfulSavingData value)? successfulSavingData,
  }) {
    return loadStatusFailure?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_Loading value)? loading,
    TResult Function(_ShowScreen value)? showScreen,
    TResult Function(_SaveChangesFailure value)? saveChangesFailure,
    TResult Function(_LoadFailure value)? loadFailure,
    TResult Function(_SuccessfulDeletionIntent value)? successfulDeletionIntent,
    TResult Function(_SuccessfulDeletionExample value)?
        successfulDeletionExample,
    TResult Function(_IntentAdded value)? intentAdded,
    TResult Function(_ShowStatus value)? showStatus,
    TResult Function(_LoadStatusFailure value)? loadStatusFailure,
    TResult Function(_SuccessfulSavingData value)? successfulSavingData,
    required TResult orElse(),
  }) {
    if (loadStatusFailure != null) {
      return loadStatusFailure(this);
    }
    return orElse();
  }
}

abstract class _LoadStatusFailure implements MainState {
  const factory _LoadStatusFailure() = _$_LoadStatusFailure;
}

/// @nodoc
abstract class _$SuccessfulSavingDataCopyWith<$Res> {
  factory _$SuccessfulSavingDataCopyWith(_SuccessfulSavingData value,
          $Res Function(_SuccessfulSavingData) then) =
      __$SuccessfulSavingDataCopyWithImpl<$Res>;
}

/// @nodoc
class __$SuccessfulSavingDataCopyWithImpl<$Res>
    extends _$MainStateCopyWithImpl<$Res>
    implements _$SuccessfulSavingDataCopyWith<$Res> {
  __$SuccessfulSavingDataCopyWithImpl(
      _SuccessfulSavingData _value, $Res Function(_SuccessfulSavingData) _then)
      : super(_value, (v) => _then(v as _SuccessfulSavingData));

  @override
  _SuccessfulSavingData get _value => super._value as _SuccessfulSavingData;
}

/// @nodoc

class _$_SuccessfulSavingData implements _SuccessfulSavingData {
  const _$_SuccessfulSavingData();

  @override
  String toString() {
    return 'MainState.successfulSavingData()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _SuccessfulSavingData);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(List<Intent> intents, bool isEnabledToSave)
        showScreen,
    required TResult Function(IntentFailure intentFailure) saveChangesFailure,
    required TResult Function(IntentFailure intentFailure) loadFailure,
    required TResult Function() successfulDeletionIntent,
    required TResult Function() successfulDeletionExample,
    required TResult Function() intentAdded,
    required TResult Function(String status) showStatus,
    required TResult Function() loadStatusFailure,
    required TResult Function() successfulSavingData,
  }) {
    return successfulSavingData();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(List<Intent> intents, bool isEnabledToSave)? showScreen,
    TResult Function(IntentFailure intentFailure)? saveChangesFailure,
    TResult Function(IntentFailure intentFailure)? loadFailure,
    TResult Function()? successfulDeletionIntent,
    TResult Function()? successfulDeletionExample,
    TResult Function()? intentAdded,
    TResult Function(String status)? showStatus,
    TResult Function()? loadStatusFailure,
    TResult Function()? successfulSavingData,
  }) {
    return successfulSavingData?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(List<Intent> intents, bool isEnabledToSave)? showScreen,
    TResult Function(IntentFailure intentFailure)? saveChangesFailure,
    TResult Function(IntentFailure intentFailure)? loadFailure,
    TResult Function()? successfulDeletionIntent,
    TResult Function()? successfulDeletionExample,
    TResult Function()? intentAdded,
    TResult Function(String status)? showStatus,
    TResult Function()? loadStatusFailure,
    TResult Function()? successfulSavingData,
    required TResult orElse(),
  }) {
    if (successfulSavingData != null) {
      return successfulSavingData();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_Loading value) loading,
    required TResult Function(_ShowScreen value) showScreen,
    required TResult Function(_SaveChangesFailure value) saveChangesFailure,
    required TResult Function(_LoadFailure value) loadFailure,
    required TResult Function(_SuccessfulDeletionIntent value)
        successfulDeletionIntent,
    required TResult Function(_SuccessfulDeletionExample value)
        successfulDeletionExample,
    required TResult Function(_IntentAdded value) intentAdded,
    required TResult Function(_ShowStatus value) showStatus,
    required TResult Function(_LoadStatusFailure value) loadStatusFailure,
    required TResult Function(_SuccessfulSavingData value) successfulSavingData,
  }) {
    return successfulSavingData(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_Loading value)? loading,
    TResult Function(_ShowScreen value)? showScreen,
    TResult Function(_SaveChangesFailure value)? saveChangesFailure,
    TResult Function(_LoadFailure value)? loadFailure,
    TResult Function(_SuccessfulDeletionIntent value)? successfulDeletionIntent,
    TResult Function(_SuccessfulDeletionExample value)?
        successfulDeletionExample,
    TResult Function(_IntentAdded value)? intentAdded,
    TResult Function(_ShowStatus value)? showStatus,
    TResult Function(_LoadStatusFailure value)? loadStatusFailure,
    TResult Function(_SuccessfulSavingData value)? successfulSavingData,
  }) {
    return successfulSavingData?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_Loading value)? loading,
    TResult Function(_ShowScreen value)? showScreen,
    TResult Function(_SaveChangesFailure value)? saveChangesFailure,
    TResult Function(_LoadFailure value)? loadFailure,
    TResult Function(_SuccessfulDeletionIntent value)? successfulDeletionIntent,
    TResult Function(_SuccessfulDeletionExample value)?
        successfulDeletionExample,
    TResult Function(_IntentAdded value)? intentAdded,
    TResult Function(_ShowStatus value)? showStatus,
    TResult Function(_LoadStatusFailure value)? loadStatusFailure,
    TResult Function(_SuccessfulSavingData value)? successfulSavingData,
    required TResult orElse(),
  }) {
    if (successfulSavingData != null) {
      return successfulSavingData(this);
    }
    return orElse();
  }
}

abstract class _SuccessfulSavingData implements MainState {
  const factory _SuccessfulSavingData() = _$_SuccessfulSavingData;
}
