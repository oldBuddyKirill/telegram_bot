part of 'form_bloc.dart';

@freezed
abstract class TextFormEvent with _$TextFormEvent {
  const factory TextFormEvent.initialized(String value) = _Initialized;
  const factory TextFormEvent.bodyChanged(String bodyStr) = _BodyChanged;
}
