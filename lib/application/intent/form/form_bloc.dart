import 'package:bloc/bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'form_event.dart';
part 'form_state.dart';
part 'form_bloc.freezed.dart';

class TextFormBloc extends Bloc<TextFormEvent, TextFormState> {
  TextFormBloc() : super(TextFormState.initial()) {
    on<TextFormEvent>((event, emit) {
      event.map(
        initialized: (_Initialized event) {
          emit(state.copyWith(
            value: event.value,
          ));
        },
        bodyChanged: (_BodyChanged event) {
          emit(state.copyWith(
            value: event.bodyStr,
          ));
        },
      );
    });
  }
}
