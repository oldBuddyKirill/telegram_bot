part of 'form_bloc.dart';

@freezed
abstract class TextFormState with _$TextFormState {
  const factory TextFormState(String value) = _TextFormState;

  factory TextFormState.initial() => const TextFormState('');
}
