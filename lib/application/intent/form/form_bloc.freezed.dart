// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'form_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
class _$TextFormEventTearOff {
  const _$TextFormEventTearOff();

  _Initialized initialized(String value) {
    return _Initialized(
      value,
    );
  }

  _BodyChanged bodyChanged(String bodyStr) {
    return _BodyChanged(
      bodyStr,
    );
  }
}

/// @nodoc
const $TextFormEvent = _$TextFormEventTearOff();

/// @nodoc
mixin _$TextFormEvent {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String value) initialized,
    required TResult Function(String bodyStr) bodyChanged,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(String value)? initialized,
    TResult Function(String bodyStr)? bodyChanged,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String value)? initialized,
    TResult Function(String bodyStr)? bodyChanged,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initialized value) initialized,
    required TResult Function(_BodyChanged value) bodyChanged,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_Initialized value)? initialized,
    TResult Function(_BodyChanged value)? bodyChanged,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initialized value)? initialized,
    TResult Function(_BodyChanged value)? bodyChanged,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $TextFormEventCopyWith<$Res> {
  factory $TextFormEventCopyWith(
          TextFormEvent value, $Res Function(TextFormEvent) then) =
      _$TextFormEventCopyWithImpl<$Res>;
}

/// @nodoc
class _$TextFormEventCopyWithImpl<$Res>
    implements $TextFormEventCopyWith<$Res> {
  _$TextFormEventCopyWithImpl(this._value, this._then);

  final TextFormEvent _value;
  // ignore: unused_field
  final $Res Function(TextFormEvent) _then;
}

/// @nodoc
abstract class _$InitializedCopyWith<$Res> {
  factory _$InitializedCopyWith(
          _Initialized value, $Res Function(_Initialized) then) =
      __$InitializedCopyWithImpl<$Res>;
  $Res call({String value});
}

/// @nodoc
class __$InitializedCopyWithImpl<$Res> extends _$TextFormEventCopyWithImpl<$Res>
    implements _$InitializedCopyWith<$Res> {
  __$InitializedCopyWithImpl(
      _Initialized _value, $Res Function(_Initialized) _then)
      : super(_value, (v) => _then(v as _Initialized));

  @override
  _Initialized get _value => super._value as _Initialized;

  @override
  $Res call({
    Object? value = freezed,
  }) {
    return _then(_Initialized(
      value == freezed
          ? _value.value
          : value // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$_Initialized implements _Initialized {
  const _$_Initialized(this.value);

  @override
  final String value;

  @override
  String toString() {
    return 'TextFormEvent.initialized(value: $value)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _Initialized &&
            const DeepCollectionEquality().equals(other.value, value));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(value));

  @JsonKey(ignore: true)
  @override
  _$InitializedCopyWith<_Initialized> get copyWith =>
      __$InitializedCopyWithImpl<_Initialized>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String value) initialized,
    required TResult Function(String bodyStr) bodyChanged,
  }) {
    return initialized(value);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(String value)? initialized,
    TResult Function(String bodyStr)? bodyChanged,
  }) {
    return initialized?.call(value);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String value)? initialized,
    TResult Function(String bodyStr)? bodyChanged,
    required TResult orElse(),
  }) {
    if (initialized != null) {
      return initialized(value);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initialized value) initialized,
    required TResult Function(_BodyChanged value) bodyChanged,
  }) {
    return initialized(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_Initialized value)? initialized,
    TResult Function(_BodyChanged value)? bodyChanged,
  }) {
    return initialized?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initialized value)? initialized,
    TResult Function(_BodyChanged value)? bodyChanged,
    required TResult orElse(),
  }) {
    if (initialized != null) {
      return initialized(this);
    }
    return orElse();
  }
}

abstract class _Initialized implements TextFormEvent {
  const factory _Initialized(String value) = _$_Initialized;

  String get value;
  @JsonKey(ignore: true)
  _$InitializedCopyWith<_Initialized> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$BodyChangedCopyWith<$Res> {
  factory _$BodyChangedCopyWith(
          _BodyChanged value, $Res Function(_BodyChanged) then) =
      __$BodyChangedCopyWithImpl<$Res>;
  $Res call({String bodyStr});
}

/// @nodoc
class __$BodyChangedCopyWithImpl<$Res> extends _$TextFormEventCopyWithImpl<$Res>
    implements _$BodyChangedCopyWith<$Res> {
  __$BodyChangedCopyWithImpl(
      _BodyChanged _value, $Res Function(_BodyChanged) _then)
      : super(_value, (v) => _then(v as _BodyChanged));

  @override
  _BodyChanged get _value => super._value as _BodyChanged;

  @override
  $Res call({
    Object? bodyStr = freezed,
  }) {
    return _then(_BodyChanged(
      bodyStr == freezed
          ? _value.bodyStr
          : bodyStr // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$_BodyChanged implements _BodyChanged {
  const _$_BodyChanged(this.bodyStr);

  @override
  final String bodyStr;

  @override
  String toString() {
    return 'TextFormEvent.bodyChanged(bodyStr: $bodyStr)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _BodyChanged &&
            const DeepCollectionEquality().equals(other.bodyStr, bodyStr));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(bodyStr));

  @JsonKey(ignore: true)
  @override
  _$BodyChangedCopyWith<_BodyChanged> get copyWith =>
      __$BodyChangedCopyWithImpl<_BodyChanged>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String value) initialized,
    required TResult Function(String bodyStr) bodyChanged,
  }) {
    return bodyChanged(bodyStr);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(String value)? initialized,
    TResult Function(String bodyStr)? bodyChanged,
  }) {
    return bodyChanged?.call(bodyStr);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String value)? initialized,
    TResult Function(String bodyStr)? bodyChanged,
    required TResult orElse(),
  }) {
    if (bodyChanged != null) {
      return bodyChanged(bodyStr);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initialized value) initialized,
    required TResult Function(_BodyChanged value) bodyChanged,
  }) {
    return bodyChanged(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_Initialized value)? initialized,
    TResult Function(_BodyChanged value)? bodyChanged,
  }) {
    return bodyChanged?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initialized value)? initialized,
    TResult Function(_BodyChanged value)? bodyChanged,
    required TResult orElse(),
  }) {
    if (bodyChanged != null) {
      return bodyChanged(this);
    }
    return orElse();
  }
}

abstract class _BodyChanged implements TextFormEvent {
  const factory _BodyChanged(String bodyStr) = _$_BodyChanged;

  String get bodyStr;
  @JsonKey(ignore: true)
  _$BodyChangedCopyWith<_BodyChanged> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
class _$TextFormStateTearOff {
  const _$TextFormStateTearOff();

  _TextFormState call(String value) {
    return _TextFormState(
      value,
    );
  }
}

/// @nodoc
const $TextFormState = _$TextFormStateTearOff();

/// @nodoc
mixin _$TextFormState {
  String get value => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $TextFormStateCopyWith<TextFormState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $TextFormStateCopyWith<$Res> {
  factory $TextFormStateCopyWith(
          TextFormState value, $Res Function(TextFormState) then) =
      _$TextFormStateCopyWithImpl<$Res>;
  $Res call({String value});
}

/// @nodoc
class _$TextFormStateCopyWithImpl<$Res>
    implements $TextFormStateCopyWith<$Res> {
  _$TextFormStateCopyWithImpl(this._value, this._then);

  final TextFormState _value;
  // ignore: unused_field
  final $Res Function(TextFormState) _then;

  @override
  $Res call({
    Object? value = freezed,
  }) {
    return _then(_value.copyWith(
      value: value == freezed
          ? _value.value
          : value // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc
abstract class _$TextFormStateCopyWith<$Res>
    implements $TextFormStateCopyWith<$Res> {
  factory _$TextFormStateCopyWith(
          _TextFormState value, $Res Function(_TextFormState) then) =
      __$TextFormStateCopyWithImpl<$Res>;
  @override
  $Res call({String value});
}

/// @nodoc
class __$TextFormStateCopyWithImpl<$Res>
    extends _$TextFormStateCopyWithImpl<$Res>
    implements _$TextFormStateCopyWith<$Res> {
  __$TextFormStateCopyWithImpl(
      _TextFormState _value, $Res Function(_TextFormState) _then)
      : super(_value, (v) => _then(v as _TextFormState));

  @override
  _TextFormState get _value => super._value as _TextFormState;

  @override
  $Res call({
    Object? value = freezed,
  }) {
    return _then(_TextFormState(
      value == freezed
          ? _value.value
          : value // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$_TextFormState implements _TextFormState {
  const _$_TextFormState(this.value);

  @override
  final String value;

  @override
  String toString() {
    return 'TextFormState(value: $value)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _TextFormState &&
            const DeepCollectionEquality().equals(other.value, value));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(value));

  @JsonKey(ignore: true)
  @override
  _$TextFormStateCopyWith<_TextFormState> get copyWith =>
      __$TextFormStateCopyWithImpl<_TextFormState>(this, _$identity);
}

abstract class _TextFormState implements TextFormState {
  const factory _TextFormState(String value) = _$_TextFormState;

  @override
  String get value;
  @override
  @JsonKey(ignore: true)
  _$TextFormStateCopyWith<_TextFormState> get copyWith =>
      throw _privateConstructorUsedError;
}
