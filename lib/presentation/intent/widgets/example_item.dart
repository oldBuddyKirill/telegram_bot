import 'package:flutter/material.dart' hide Intent;
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:telegram_bot/application/intent/main/main_bloc.dart';
import 'package:telegram_bot/domain/intent/intent.dart';
import 'package:telegram_bot/presentation/widgets/bloc_text_field.dart';

class ExampleItem extends StatelessWidget{
  final Intent intent;
  final int exampleIndex;
  const ExampleItem({Key? key, required this.intent, required this.exampleIndex}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 5),
      child: Row(
        children: [
          Expanded(
            child: BlocTextField<MainBloc, MainState, String>(
              bloc: context.read<MainBloc>(),
              onChanged: (value) {
                context.read<MainBloc>().add(MainEvent.exampleChanged(
                  example: value ?? '',
                  intent: intent,
                  index: exampleIndex,
                ));
              },
              stateValue: (state) => state.maybeMap(
                showScreen: (s) {
                  try {
                    return s.intents
                        .firstWhere((element) => element.intentBody == intent.intentBody)
                        .examples[exampleIndex]
                        .getOrCrash();
                  } catch (_) {
                    return '';
                  }
                },
                orElse: () {
                  return '';
                },
              ),
              inputToValue: (String? input) => input,
              textAlign: TextAlign.center,
              maxLines: null,
              decoration: InputDecoration(
                disabledBorder: InputBorder.none,
                contentPadding: const EdgeInsets.all(5),
                enabledBorder: OutlineInputBorder(
                  borderSide: const BorderSide(
                    width: 2,
                    color: Colors.grey,
                  ),
                  borderRadius: BorderRadius.circular(15),
                ),
                focusedBorder: OutlineInputBorder(
                  borderSide: const BorderSide(
                    width: 2,
                    color: Colors.green,
                  ),
                  borderRadius: BorderRadius.circular(15),
                ),
              ),
            ),
          ),
          IconButton(
            padding: EdgeInsets.zero,
            onPressed: () {
              context.read<MainBloc>().add(
                MainEvent.deleteExample(
                  exampleIndex: exampleIndex,
                  intent: intent,
                ),
              );
            },
            icon: const Icon(Icons.delete_forever),
            color: Colors.red,
          )
        ],
      ),
    );
  }

}