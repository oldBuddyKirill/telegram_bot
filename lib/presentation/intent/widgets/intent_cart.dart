import 'package:flutter/material.dart' hide Intent;
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:telegram_bot/application/intent/main/main_bloc.dart';
import 'package:telegram_bot/domain/intent/intent.dart';
import 'package:telegram_bot/presentation/intent/widgets/editing_dialog.dart';
import 'package:telegram_bot/presentation/intent/widgets/example_item.dart';

class IntentCart extends StatelessWidget {
  final Intent intent;

  const IntentCart({Key? key, required this.intent}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        _listTile(context),
        const SizedBox(height: 15),
        const Text('EXAMPLES:'),
        const SizedBox(height: 5),
        ListView.builder(
          shrinkWrap: true, // необходимо, чтобы ListView корректно отображался в другом ListView
          itemCount: intent.examples.length,
          itemBuilder: (context, index) => ExampleItem(intent: intent, exampleIndex: index),
          physics: const NeverScrollableScrollPhysics(),
        ),
        const SizedBox(height: 5),
        FloatingActionButton(
          onPressed: () {
            context.read<MainBloc>().add(MainEvent.addExample(intent));
          },
          child: const Icon(Icons.add, size: 24),
          mini: true,
        ),
        const SizedBox(height: 15),
      ],
    );
  }

  _confirmDialog(BuildContext context) async {
    return await showDialog(
      context: context,
      builder: (context) => AlertDialog(
        title: const Text('Delete Intent?'),
        content: Text(
          intent.intentBody.getOrCrash(),
          style: const TextStyle(fontSize: 20),
        ),
        actions: [
          TextButton(
            onPressed: () {
              context.read<MainBloc>().add(MainEvent.deleteIntent(intent));
              Navigator.pop(context);
            },
            child: const Text('YES', style: TextStyle(fontSize: 16)),
          ),
          TextButton(
            onPressed: () => Navigator.pop(context),
            child: const Text(
              'NO',
              style: TextStyle(fontSize: 16),
            ),
          ),
        ],
      ),
    );
  }

  _listTile(BuildContext context) {
    return ListTile(
      style: ListTileStyle.drawer,
      contentPadding: const EdgeInsets.only(left: 40, right: 15),
      title: Center(
        child: Text(
          intent.intentBody.getOrCrash(),
          style: const TextStyle(fontSize: 20),
        ),
      ),
      trailing: PopupMenuButton(
        icon: const Icon(Icons.more_vert, color: Colors.black),
        iconSize: 20,
        itemBuilder: (context) => <PopupMenuEntry<String>>[
          const PopupMenuItem(
            value: 'edit',
            child: ListTile(
              contentPadding: EdgeInsets.zero,
              leading: Icon(Icons.edit, size: 20),
              title: Text('Edit'),
            ),
          ),
          const PopupMenuItem(
            value: 'delete',
            child: ListTile(
              contentPadding: EdgeInsets.zero,
              leading: Icon(Icons.delete_forever, size: 20),
              title: Text('Delete'),
            ),
          ),
        ],
        onSelected: (value) {
          switch (value) {
            case 'edit':
              editingDialog(
                context: context,
                intent: intent,
                title: 'Editing',
              );
              break;
            case 'delete':
              _confirmDialog(context);
              break;
            default:
              const Center(child: Text('Unhandled error in Intent cart'));
          }
        },
      ),
    );
  }
}
