import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:telegram_bot/application/intent/main/main_bloc.dart';
import 'package:telegram_bot/presentation/intent/widgets/intent_cart.dart';
import 'package:telegram_bot/presentation/intent/widgets/refresh_button.dart';

class IntentPageBody extends StatelessWidget {
  IntentPageBody({Key? key}) : super(key: key);

  final _controller = ScrollController();

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<MainBloc, MainState>(
      buildWhen: (p, c) {
        return c.maybeMap(
          showScreen: (s) => true,
          loading: (s) => true,
          loadFailure: (s) => true,
          orElse: () => false,
        );
      },
      listenWhen: (p, c) => p == const MainState.intentAdded(),
      listener: (context, state) {
        _controller.animateTo(
          _controller.position.maxScrollExtent,
          duration: const Duration(milliseconds: 500),
          curve: Curves.ease,
        );
      },
      builder: (context, state) {
        return state.maybeMap(
          loading: (_) => const Center(child: CircularProgressIndicator()),
          loadFailure: (_) => RefreshButton(context: context),
          showScreen: (state) {
            return Center(
              child: ListView.builder(
                controller: _controller,
                itemCount: state.intents.length,
                itemBuilder: (context, index) => IntentCart(intent: state.intents[index]),
                physics: const BouncingScrollPhysics(),
                // controller: ,
              ),
            );
          },
          orElse: () {
            return const Center(child: Text('Вы не должны были видеть этот текст'));
          },
        );
      },
    );
  }
}
