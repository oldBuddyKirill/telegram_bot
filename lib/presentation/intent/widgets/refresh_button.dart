import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:telegram_bot/application/intent/main/main_bloc.dart';

class RefreshButton extends Center {
  RefreshButton({Key? key, required BuildContext context})
      : super(
          key: key,
          child: SizedBox(
            height: 46,
            child: TextButton(
              onPressed: () {
                context.read<MainBloc>().add(const MainEvent.loadIntents());
              },
              child: const Text('Try again'),
              style: TextButton.styleFrom(
                padding: const EdgeInsets.symmetric(horizontal: 20),
                elevation: 0,
                backgroundColor: Colors.green,
                textStyle: const TextStyle(fontSize: 20),
                primary: Colors.white,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(14),
                  side: BorderSide.none,
                ),
              ),
            ),
          ),
        );
}
