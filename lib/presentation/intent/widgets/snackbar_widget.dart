import 'package:flutter/material.dart';

class MySnackBar extends SnackBar {
  final String message;
  final Color? myBackgroundColor;

  MySnackBar({
    Key? key,
    required this.message,
    this.myBackgroundColor,
  }) : super(
          key: key,
          content: Text(message),
          // shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
          backgroundColor: myBackgroundColor,
          duration: const Duration(seconds: 3),
          // behavior: SnackBarBehavior.,
          action: SnackBarAction(
            textColor: myBackgroundColor == null ? null : Colors.white,
            label: 'ОК',
            onPressed: () {},
          ),
        );
}
