import 'package:flutter/material.dart' hide Intent;
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:telegram_bot/application/intent/form/form_bloc.dart';
import 'package:telegram_bot/application/intent/main/main_bloc.dart';
import 'package:telegram_bot/domain/intent/intent.dart';
import 'package:telegram_bot/domain/intent/value_objects.dart';
import 'package:telegram_bot/presentation/widgets/bloc_text_field.dart';

Future editingDialog({
  required BuildContext context,
  required Intent intent,
  required String title,
  bool isNew = false,
}) async {
  return await showDialog(
    context: context,
    builder: (context) => BlocProvider<TextFormBloc>(
      create: (context) => TextFormBloc(),
      child: Builder(builder: (context) {
        return AlertDialog(
          actionsAlignment: MainAxisAlignment.center,
          title: Text(title),
          content: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              BlocTextField<TextFormBloc, TextFormState, String>(
                decoration: const InputDecoration(
                  contentPadding: EdgeInsets.symmetric(horizontal: 5),
                  enabledBorder: UnderlineInputBorder(
                    borderSide: BorderSide(
                      width: 2,
                      color: Colors.grey,
                    ),
                  ),
                  focusedBorder: UnderlineInputBorder(
                    borderSide: BorderSide(
                      width: 2,
                      color: Colors.green,
                    ),
                  ),
                ),
                bloc: context.read<TextFormBloc>()
                  ..add(TextFormEvent.initialized(intent.intentBody.value.getOrElse(() => ''))),
                stateValue: (state) {
                  return state.value;
                },
                onChanged: (value) {
                  context.read<TextFormBloc>().add(TextFormEvent.bodyChanged(value ?? ''));
                },
                inputToValue: (String? input) => input,
              ),
            ],
          ),
          actions: [
            TextButton(
              onPressed: () => Navigator.pop(context),
              child: const Text('Cancel', style: TextStyle(fontSize: 16)),
            ),
            BlocBuilder<TextFormBloc, TextFormState>(builder: (context, state) {
              late final List<Intent> _intents;
              context.read<MainBloc>().state.maybeMap(
                    showScreen: (s) {
                      _intents = s.intents;
                    },
                    orElse: () {},
                  );
              bool isEnabled = state.value.trim().isNotEmpty &&
                  _intents.every((element) => element.intentBody.getOrCrash() != state.value.trim());
              return TextButton(
                onPressed: !isEnabled
                    ? null
                    : () {
                        isNew
                            ? context
                                .read<MainBloc>()
                                .add(MainEvent.addIntent(intent.copyWith(intentBody: IntentBody(state.value))))
                            : context
                                .read<MainBloc>()
                                .add(MainEvent.intentChanged(intent.intentBody.getOrCrash(), state.value));
                        Navigator.pop(context);
                      },
                child: const Text('Save', style: TextStyle(fontSize: 16)),
              );
            }),
          ],
        );
      }),
    ),
  );
}
