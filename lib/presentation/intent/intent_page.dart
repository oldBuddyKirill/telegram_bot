import 'package:flutter/material.dart' hide Intent;
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:telegram_bot/application/intent/main/main_bloc.dart';
import 'package:telegram_bot/domain/intent/intent.dart';
import 'package:telegram_bot/domain/intent/intent_failure.dart';
import 'package:telegram_bot/domain/intent/value_objects.dart';
import 'package:telegram_bot/presentation/intent/widgets/editing_dialog.dart';
import 'package:telegram_bot/presentation/intent/widgets/intent_page_body_widget.dart';
import 'package:telegram_bot/presentation/intent/widgets/snackbar_widget.dart';

class IntentPage extends StatelessWidget {
  const IntentPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocListener<MainBloc, MainState>(
      listener: (context, state) {
        state.maybeMap(
          loadFailure: (state) {
            ScaffoldMessenger.of(context).showSnackBar(MySnackBar(
              message: 'An error occurred while loading data',
              myBackgroundColor: Colors.red,
            ));
          },
          successfulDeletionExample: (state) {
            ScaffoldMessenger.of(context).showSnackBar(MySnackBar(message: 'Example deleted  successfully'));
          },
          successfulDeletionIntent: (state) {
            ScaffoldMessenger.of(context).showSnackBar(MySnackBar(message: 'Intent deleted  successfully'));
          },
          saveChangesFailure: (state) {
            ScaffoldMessenger.of(context).showSnackBar(MySnackBar(
              message: 'An error occurred while sending data',
              myBackgroundColor: Colors.red,
            ));
          },
          showStatus: (state) {
            ScaffoldMessenger.of(context).showSnackBar(MySnackBar(
              message: 'Training status: ${state.status.toUpperCase()}',
            ));
          },
          successfulSavingData: (state) {
            ScaffoldMessenger.of(context).showSnackBar(MySnackBar(
              message: 'Data saved successfully',
              myBackgroundColor: Colors.green,
            ));
          },
          orElse: () {},
        );
      },
      child: Scaffold(
        appBar: AppBar(
          title: const Text('Telegram Bot'),
          centerTitle: true,
          leading: Row(
            children: [
              IconButton(
                onPressed: () => context.read<MainBloc>().state == const MainState.loading()
                    ? null
                    : context.read<MainBloc>().add(const MainEvent.loadIntents()),
                icon: const Icon(Icons.refresh),
                iconSize: 22,
              ),
            ],
          ),
          actions: [
            IconButton(
              onPressed: () {
                context.read<MainBloc>().state == const MainState.loading()
                    ? null
                    : context.read<MainBloc>().add(const MainEvent.loadStatus());
              },
              icon: const Icon(Icons.run_circle_outlined),
              iconSize: 22,
            ),
            IconButton(
              onPressed: () {
                if (context.read<MainBloc>().state != const MainState.loading() &&
                    context.read<MainBloc>().state != const MainState.loadFailure(IntentFailure.serverError())) {
                  context.read<MainBloc>().state.maybeMap(
                    showScreen: (state) {
                      return state.isEnabledToSave
                          ? context.read<MainBloc>().add(const MainEvent.saveChanges())
                          : ScaffoldMessenger.of(context).showSnackBar(MySnackBar(
                              message: 'An empty Example or Intent with no examples present',
                              myBackgroundColor: Colors.red,
                            ));
                    },
                    orElse: () {
                      return null;
                    },
                  );
                }
              },
              icon: const Icon(Icons.save_alt),
              iconSize: 22,
            ),
            const SizedBox(width: 15),
          ],
        ),
        body: IntentPageBody(),
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            if (context.read<MainBloc>().state != const MainState.loading() &&
                context.read<MainBloc>().state != const MainState.loadFailure(IntentFailure.serverError())) {
              final Intent newIntent = Intent.empty().copyWith(examples: [Example('')]);
              editingDialog(
                context: context,
                intent: newIntent,
                title: 'Creating Intention',
                isNew: true,
              );
            }
          },
          child: const Text(' Add\nIntent', style: TextStyle(fontSize: 13)),
          backgroundColor: Colors.green[800],
        ),
      ),
    );
  }
}
