import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:telegram_bot/application/intent/main/main_bloc.dart';
import 'package:telegram_bot/infrastructure/intent/intent_repository.dart';
import 'package:telegram_bot/presentation/intent/intent_page.dart';

class AppWidget extends StatelessWidget {
  AppWidget({Key? key}) : super(key: key);

  final IntentRepository _intentRepository = IntentRepository();

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => MainBloc(_intentRepository)..add(const MainEvent.loadIntents()),
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          primarySwatch: Colors.green
        ),
        home: const IntentPage(),
      ),
    );
  }
}
