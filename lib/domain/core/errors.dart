import 'package:telegram_bot/domain/core/failures.dart';

class UnexpectedValueError extends Error {
  final ValueFailure valueFailure;

  UnexpectedValueError(this.valueFailure);

  @override
  String toString() {
    return Error.safeToString('Encountered a ValueFailure at an unrecoverable point. Failure: $valueFailure');
  }
}