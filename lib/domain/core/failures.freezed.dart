// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'failures.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
class _$ValueFailureTearOff {
  const _$ValueFailureTearOff();

  InvalidIntent<T> invalidIntent<T>({required String failedValue}) {
    return InvalidIntent<T>(
      failedValue: failedValue,
    );
  }

  EmptyExample<T> emptyExample<T>({required String failedValue}) {
    return EmptyExample<T>(
      failedValue: failedValue,
    );
  }
}

/// @nodoc
const $ValueFailure = _$ValueFailureTearOff();

/// @nodoc
mixin _$ValueFailure<T> {
  String get failedValue => throw _privateConstructorUsedError;

  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String failedValue) invalidIntent,
    required TResult Function(String failedValue) emptyExample,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(String failedValue)? invalidIntent,
    TResult Function(String failedValue)? emptyExample,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String failedValue)? invalidIntent,
    TResult Function(String failedValue)? emptyExample,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(InvalidIntent<T> value) invalidIntent,
    required TResult Function(EmptyExample<T> value) emptyExample,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(InvalidIntent<T> value)? invalidIntent,
    TResult Function(EmptyExample<T> value)? emptyExample,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(InvalidIntent<T> value)? invalidIntent,
    TResult Function(EmptyExample<T> value)? emptyExample,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $ValueFailureCopyWith<T, ValueFailure<T>> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ValueFailureCopyWith<T, $Res> {
  factory $ValueFailureCopyWith(
          ValueFailure<T> value, $Res Function(ValueFailure<T>) then) =
      _$ValueFailureCopyWithImpl<T, $Res>;
  $Res call({String failedValue});
}

/// @nodoc
class _$ValueFailureCopyWithImpl<T, $Res>
    implements $ValueFailureCopyWith<T, $Res> {
  _$ValueFailureCopyWithImpl(this._value, this._then);

  final ValueFailure<T> _value;
  // ignore: unused_field
  final $Res Function(ValueFailure<T>) _then;

  @override
  $Res call({
    Object? failedValue = freezed,
  }) {
    return _then(_value.copyWith(
      failedValue: failedValue == freezed
          ? _value.failedValue
          : failedValue // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc
abstract class $InvalidIntentCopyWith<T, $Res>
    implements $ValueFailureCopyWith<T, $Res> {
  factory $InvalidIntentCopyWith(
          InvalidIntent<T> value, $Res Function(InvalidIntent<T>) then) =
      _$InvalidIntentCopyWithImpl<T, $Res>;
  @override
  $Res call({String failedValue});
}

/// @nodoc
class _$InvalidIntentCopyWithImpl<T, $Res>
    extends _$ValueFailureCopyWithImpl<T, $Res>
    implements $InvalidIntentCopyWith<T, $Res> {
  _$InvalidIntentCopyWithImpl(
      InvalidIntent<T> _value, $Res Function(InvalidIntent<T>) _then)
      : super(_value, (v) => _then(v as InvalidIntent<T>));

  @override
  InvalidIntent<T> get _value => super._value as InvalidIntent<T>;

  @override
  $Res call({
    Object? failedValue = freezed,
  }) {
    return _then(InvalidIntent<T>(
      failedValue: failedValue == freezed
          ? _value.failedValue
          : failedValue // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$InvalidIntent<T> implements InvalidIntent<T> {
  const _$InvalidIntent({required this.failedValue});

  @override
  final String failedValue;

  @override
  String toString() {
    return 'ValueFailure<$T>.invalidIntent(failedValue: $failedValue)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is InvalidIntent<T> &&
            const DeepCollectionEquality()
                .equals(other.failedValue, failedValue));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType, const DeepCollectionEquality().hash(failedValue));

  @JsonKey(ignore: true)
  @override
  $InvalidIntentCopyWith<T, InvalidIntent<T>> get copyWith =>
      _$InvalidIntentCopyWithImpl<T, InvalidIntent<T>>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String failedValue) invalidIntent,
    required TResult Function(String failedValue) emptyExample,
  }) {
    return invalidIntent(failedValue);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(String failedValue)? invalidIntent,
    TResult Function(String failedValue)? emptyExample,
  }) {
    return invalidIntent?.call(failedValue);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String failedValue)? invalidIntent,
    TResult Function(String failedValue)? emptyExample,
    required TResult orElse(),
  }) {
    if (invalidIntent != null) {
      return invalidIntent(failedValue);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(InvalidIntent<T> value) invalidIntent,
    required TResult Function(EmptyExample<T> value) emptyExample,
  }) {
    return invalidIntent(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(InvalidIntent<T> value)? invalidIntent,
    TResult Function(EmptyExample<T> value)? emptyExample,
  }) {
    return invalidIntent?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(InvalidIntent<T> value)? invalidIntent,
    TResult Function(EmptyExample<T> value)? emptyExample,
    required TResult orElse(),
  }) {
    if (invalidIntent != null) {
      return invalidIntent(this);
    }
    return orElse();
  }
}

abstract class InvalidIntent<T> implements ValueFailure<T> {
  const factory InvalidIntent({required String failedValue}) =
      _$InvalidIntent<T>;

  @override
  String get failedValue;
  @override
  @JsonKey(ignore: true)
  $InvalidIntentCopyWith<T, InvalidIntent<T>> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $EmptyExampleCopyWith<T, $Res>
    implements $ValueFailureCopyWith<T, $Res> {
  factory $EmptyExampleCopyWith(
          EmptyExample<T> value, $Res Function(EmptyExample<T>) then) =
      _$EmptyExampleCopyWithImpl<T, $Res>;
  @override
  $Res call({String failedValue});
}

/// @nodoc
class _$EmptyExampleCopyWithImpl<T, $Res>
    extends _$ValueFailureCopyWithImpl<T, $Res>
    implements $EmptyExampleCopyWith<T, $Res> {
  _$EmptyExampleCopyWithImpl(
      EmptyExample<T> _value, $Res Function(EmptyExample<T>) _then)
      : super(_value, (v) => _then(v as EmptyExample<T>));

  @override
  EmptyExample<T> get _value => super._value as EmptyExample<T>;

  @override
  $Res call({
    Object? failedValue = freezed,
  }) {
    return _then(EmptyExample<T>(
      failedValue: failedValue == freezed
          ? _value.failedValue
          : failedValue // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$EmptyExample<T> implements EmptyExample<T> {
  const _$EmptyExample({required this.failedValue});

  @override
  final String failedValue;

  @override
  String toString() {
    return 'ValueFailure<$T>.emptyExample(failedValue: $failedValue)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is EmptyExample<T> &&
            const DeepCollectionEquality()
                .equals(other.failedValue, failedValue));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType, const DeepCollectionEquality().hash(failedValue));

  @JsonKey(ignore: true)
  @override
  $EmptyExampleCopyWith<T, EmptyExample<T>> get copyWith =>
      _$EmptyExampleCopyWithImpl<T, EmptyExample<T>>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String failedValue) invalidIntent,
    required TResult Function(String failedValue) emptyExample,
  }) {
    return emptyExample(failedValue);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(String failedValue)? invalidIntent,
    TResult Function(String failedValue)? emptyExample,
  }) {
    return emptyExample?.call(failedValue);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String failedValue)? invalidIntent,
    TResult Function(String failedValue)? emptyExample,
    required TResult orElse(),
  }) {
    if (emptyExample != null) {
      return emptyExample(failedValue);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(InvalidIntent<T> value) invalidIntent,
    required TResult Function(EmptyExample<T> value) emptyExample,
  }) {
    return emptyExample(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(InvalidIntent<T> value)? invalidIntent,
    TResult Function(EmptyExample<T> value)? emptyExample,
  }) {
    return emptyExample?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(InvalidIntent<T> value)? invalidIntent,
    TResult Function(EmptyExample<T> value)? emptyExample,
    required TResult orElse(),
  }) {
    if (emptyExample != null) {
      return emptyExample(this);
    }
    return orElse();
  }
}

abstract class EmptyExample<T> implements ValueFailure<T> {
  const factory EmptyExample({required String failedValue}) = _$EmptyExample<T>;

  @override
  String get failedValue;
  @override
  @JsonKey(ignore: true)
  $EmptyExampleCopyWith<T, EmptyExample<T>> get copyWith =>
      throw _privateConstructorUsedError;
}
