import 'package:freezed_annotation/freezed_annotation.dart';

part 'failures.freezed.dart';

@freezed
abstract class ValueFailure<T> with _$ValueFailure<T> {
  const factory ValueFailure.invalidIntent({
    required String failedValue,
  }) = InvalidIntent<T>;
  const factory ValueFailure.emptyExample({
    required String failedValue,
  }) = EmptyExample<T>;
}