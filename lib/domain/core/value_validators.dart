import 'package:dartz/dartz.dart';

import 'failures.dart';

Either<ValueFailure<String>, String> intentValidate(String input) {
  if (input.trim().isNotEmpty && input.length < 60) return right(input.trim());
  return left(ValueFailure.invalidIntent(failedValue: input));
}
