import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:telegram_bot/domain/intent/value_objects.dart';

part 'intent.freezed.dart';

@freezed
abstract class Intent implements _$Intent {
  const Intent._();

  const factory Intent({
    required IntentBody intentBody,
    required List<Example> examples,
  }) = _Intent;

  factory Intent.empty() => Intent(intentBody: IntentBody(''), examples: <Example>[]);
}
