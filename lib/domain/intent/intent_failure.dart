import 'package:freezed_annotation/freezed_annotation.dart';

part 'intent_failure.freezed.dart';

@freezed
abstract class IntentFailure with _$IntentFailure{
  const factory IntentFailure.serverError() = ServerError;
  const factory IntentFailure.intentAlreadyUsed() = IntentAlreadyUsed;
  const factory IntentFailure.exampleAlreadyUsed() = ExampleAlreadyUsed;
  const factory IntentFailure.invalidIntentOrExample() = InvalidIntentOrExample;
}