// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'intent.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
class _$IntentTearOff {
  const _$IntentTearOff();

  _Intent call(
      {required IntentBody intentBody, required List<Example> examples}) {
    return _Intent(
      intentBody: intentBody,
      examples: examples,
    );
  }
}

/// @nodoc
const $Intent = _$IntentTearOff();

/// @nodoc
mixin _$Intent {
  IntentBody get intentBody => throw _privateConstructorUsedError;
  List<Example> get examples => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $IntentCopyWith<Intent> get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $IntentCopyWith<$Res> {
  factory $IntentCopyWith(Intent value, $Res Function(Intent) then) =
      _$IntentCopyWithImpl<$Res>;
  $Res call({IntentBody intentBody, List<Example> examples});
}

/// @nodoc
class _$IntentCopyWithImpl<$Res> implements $IntentCopyWith<$Res> {
  _$IntentCopyWithImpl(this._value, this._then);

  final Intent _value;
  // ignore: unused_field
  final $Res Function(Intent) _then;

  @override
  $Res call({
    Object? intentBody = freezed,
    Object? examples = freezed,
  }) {
    return _then(_value.copyWith(
      intentBody: intentBody == freezed
          ? _value.intentBody
          : intentBody // ignore: cast_nullable_to_non_nullable
              as IntentBody,
      examples: examples == freezed
          ? _value.examples
          : examples // ignore: cast_nullable_to_non_nullable
              as List<Example>,
    ));
  }
}

/// @nodoc
abstract class _$IntentCopyWith<$Res> implements $IntentCopyWith<$Res> {
  factory _$IntentCopyWith(_Intent value, $Res Function(_Intent) then) =
      __$IntentCopyWithImpl<$Res>;
  @override
  $Res call({IntentBody intentBody, List<Example> examples});
}

/// @nodoc
class __$IntentCopyWithImpl<$Res> extends _$IntentCopyWithImpl<$Res>
    implements _$IntentCopyWith<$Res> {
  __$IntentCopyWithImpl(_Intent _value, $Res Function(_Intent) _then)
      : super(_value, (v) => _then(v as _Intent));

  @override
  _Intent get _value => super._value as _Intent;

  @override
  $Res call({
    Object? intentBody = freezed,
    Object? examples = freezed,
  }) {
    return _then(_Intent(
      intentBody: intentBody == freezed
          ? _value.intentBody
          : intentBody // ignore: cast_nullable_to_non_nullable
              as IntentBody,
      examples: examples == freezed
          ? _value.examples
          : examples // ignore: cast_nullable_to_non_nullable
              as List<Example>,
    ));
  }
}

/// @nodoc

class _$_Intent extends _Intent {
  const _$_Intent({required this.intentBody, required this.examples})
      : super._();

  @override
  final IntentBody intentBody;
  @override
  final List<Example> examples;

  @override
  String toString() {
    return 'Intent(intentBody: $intentBody, examples: $examples)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _Intent &&
            const DeepCollectionEquality()
                .equals(other.intentBody, intentBody) &&
            const DeepCollectionEquality().equals(other.examples, examples));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(intentBody),
      const DeepCollectionEquality().hash(examples));

  @JsonKey(ignore: true)
  @override
  _$IntentCopyWith<_Intent> get copyWith =>
      __$IntentCopyWithImpl<_Intent>(this, _$identity);
}

abstract class _Intent extends Intent {
  const factory _Intent(
      {required IntentBody intentBody,
      required List<Example> examples}) = _$_Intent;
  const _Intent._() : super._();

  @override
  IntentBody get intentBody;
  @override
  List<Example> get examples;
  @override
  @JsonKey(ignore: true)
  _$IntentCopyWith<_Intent> get copyWith => throw _privateConstructorUsedError;
}
