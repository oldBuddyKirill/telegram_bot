import 'package:dartz/dartz.dart';
import 'package:telegram_bot/domain/intent/intent.dart';
import 'package:telegram_bot/domain/intent/intent_failure.dart';

abstract class IIntentRepository {
  Future<Either<IntentFailure, Unit>> saveChanges(List<Intent> intents);
  Future<Either<IntentFailure, List<Intent>>> loadIntents();
  Future<Either<IntentFailure, String>> getStatus();
}