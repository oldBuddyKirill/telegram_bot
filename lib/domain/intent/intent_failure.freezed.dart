// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'intent_failure.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
class _$IntentFailureTearOff {
  const _$IntentFailureTearOff();

  ServerError serverError() {
    return const ServerError();
  }

  IntentAlreadyUsed intentAlreadyUsed() {
    return const IntentAlreadyUsed();
  }

  ExampleAlreadyUsed exampleAlreadyUsed() {
    return const ExampleAlreadyUsed();
  }

  InvalidIntentOrExample invalidIntentOrExample() {
    return const InvalidIntentOrExample();
  }
}

/// @nodoc
const $IntentFailure = _$IntentFailureTearOff();

/// @nodoc
mixin _$IntentFailure {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() serverError,
    required TResult Function() intentAlreadyUsed,
    required TResult Function() exampleAlreadyUsed,
    required TResult Function() invalidIntentOrExample,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? serverError,
    TResult Function()? intentAlreadyUsed,
    TResult Function()? exampleAlreadyUsed,
    TResult Function()? invalidIntentOrExample,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? serverError,
    TResult Function()? intentAlreadyUsed,
    TResult Function()? exampleAlreadyUsed,
    TResult Function()? invalidIntentOrExample,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ServerError value) serverError,
    required TResult Function(IntentAlreadyUsed value) intentAlreadyUsed,
    required TResult Function(ExampleAlreadyUsed value) exampleAlreadyUsed,
    required TResult Function(InvalidIntentOrExample value)
        invalidIntentOrExample,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(ServerError value)? serverError,
    TResult Function(IntentAlreadyUsed value)? intentAlreadyUsed,
    TResult Function(ExampleAlreadyUsed value)? exampleAlreadyUsed,
    TResult Function(InvalidIntentOrExample value)? invalidIntentOrExample,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ServerError value)? serverError,
    TResult Function(IntentAlreadyUsed value)? intentAlreadyUsed,
    TResult Function(ExampleAlreadyUsed value)? exampleAlreadyUsed,
    TResult Function(InvalidIntentOrExample value)? invalidIntentOrExample,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $IntentFailureCopyWith<$Res> {
  factory $IntentFailureCopyWith(
          IntentFailure value, $Res Function(IntentFailure) then) =
      _$IntentFailureCopyWithImpl<$Res>;
}

/// @nodoc
class _$IntentFailureCopyWithImpl<$Res>
    implements $IntentFailureCopyWith<$Res> {
  _$IntentFailureCopyWithImpl(this._value, this._then);

  final IntentFailure _value;
  // ignore: unused_field
  final $Res Function(IntentFailure) _then;
}

/// @nodoc
abstract class $ServerErrorCopyWith<$Res> {
  factory $ServerErrorCopyWith(
          ServerError value, $Res Function(ServerError) then) =
      _$ServerErrorCopyWithImpl<$Res>;
}

/// @nodoc
class _$ServerErrorCopyWithImpl<$Res> extends _$IntentFailureCopyWithImpl<$Res>
    implements $ServerErrorCopyWith<$Res> {
  _$ServerErrorCopyWithImpl(
      ServerError _value, $Res Function(ServerError) _then)
      : super(_value, (v) => _then(v as ServerError));

  @override
  ServerError get _value => super._value as ServerError;
}

/// @nodoc

class _$ServerError implements ServerError {
  const _$ServerError();

  @override
  String toString() {
    return 'IntentFailure.serverError()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is ServerError);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() serverError,
    required TResult Function() intentAlreadyUsed,
    required TResult Function() exampleAlreadyUsed,
    required TResult Function() invalidIntentOrExample,
  }) {
    return serverError();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? serverError,
    TResult Function()? intentAlreadyUsed,
    TResult Function()? exampleAlreadyUsed,
    TResult Function()? invalidIntentOrExample,
  }) {
    return serverError?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? serverError,
    TResult Function()? intentAlreadyUsed,
    TResult Function()? exampleAlreadyUsed,
    TResult Function()? invalidIntentOrExample,
    required TResult orElse(),
  }) {
    if (serverError != null) {
      return serverError();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ServerError value) serverError,
    required TResult Function(IntentAlreadyUsed value) intentAlreadyUsed,
    required TResult Function(ExampleAlreadyUsed value) exampleAlreadyUsed,
    required TResult Function(InvalidIntentOrExample value)
        invalidIntentOrExample,
  }) {
    return serverError(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(ServerError value)? serverError,
    TResult Function(IntentAlreadyUsed value)? intentAlreadyUsed,
    TResult Function(ExampleAlreadyUsed value)? exampleAlreadyUsed,
    TResult Function(InvalidIntentOrExample value)? invalidIntentOrExample,
  }) {
    return serverError?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ServerError value)? serverError,
    TResult Function(IntentAlreadyUsed value)? intentAlreadyUsed,
    TResult Function(ExampleAlreadyUsed value)? exampleAlreadyUsed,
    TResult Function(InvalidIntentOrExample value)? invalidIntentOrExample,
    required TResult orElse(),
  }) {
    if (serverError != null) {
      return serverError(this);
    }
    return orElse();
  }
}

abstract class ServerError implements IntentFailure {
  const factory ServerError() = _$ServerError;
}

/// @nodoc
abstract class $IntentAlreadyUsedCopyWith<$Res> {
  factory $IntentAlreadyUsedCopyWith(
          IntentAlreadyUsed value, $Res Function(IntentAlreadyUsed) then) =
      _$IntentAlreadyUsedCopyWithImpl<$Res>;
}

/// @nodoc
class _$IntentAlreadyUsedCopyWithImpl<$Res>
    extends _$IntentFailureCopyWithImpl<$Res>
    implements $IntentAlreadyUsedCopyWith<$Res> {
  _$IntentAlreadyUsedCopyWithImpl(
      IntentAlreadyUsed _value, $Res Function(IntentAlreadyUsed) _then)
      : super(_value, (v) => _then(v as IntentAlreadyUsed));

  @override
  IntentAlreadyUsed get _value => super._value as IntentAlreadyUsed;
}

/// @nodoc

class _$IntentAlreadyUsed implements IntentAlreadyUsed {
  const _$IntentAlreadyUsed();

  @override
  String toString() {
    return 'IntentFailure.intentAlreadyUsed()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is IntentAlreadyUsed);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() serverError,
    required TResult Function() intentAlreadyUsed,
    required TResult Function() exampleAlreadyUsed,
    required TResult Function() invalidIntentOrExample,
  }) {
    return intentAlreadyUsed();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? serverError,
    TResult Function()? intentAlreadyUsed,
    TResult Function()? exampleAlreadyUsed,
    TResult Function()? invalidIntentOrExample,
  }) {
    return intentAlreadyUsed?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? serverError,
    TResult Function()? intentAlreadyUsed,
    TResult Function()? exampleAlreadyUsed,
    TResult Function()? invalidIntentOrExample,
    required TResult orElse(),
  }) {
    if (intentAlreadyUsed != null) {
      return intentAlreadyUsed();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ServerError value) serverError,
    required TResult Function(IntentAlreadyUsed value) intentAlreadyUsed,
    required TResult Function(ExampleAlreadyUsed value) exampleAlreadyUsed,
    required TResult Function(InvalidIntentOrExample value)
        invalidIntentOrExample,
  }) {
    return intentAlreadyUsed(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(ServerError value)? serverError,
    TResult Function(IntentAlreadyUsed value)? intentAlreadyUsed,
    TResult Function(ExampleAlreadyUsed value)? exampleAlreadyUsed,
    TResult Function(InvalidIntentOrExample value)? invalidIntentOrExample,
  }) {
    return intentAlreadyUsed?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ServerError value)? serverError,
    TResult Function(IntentAlreadyUsed value)? intentAlreadyUsed,
    TResult Function(ExampleAlreadyUsed value)? exampleAlreadyUsed,
    TResult Function(InvalidIntentOrExample value)? invalidIntentOrExample,
    required TResult orElse(),
  }) {
    if (intentAlreadyUsed != null) {
      return intentAlreadyUsed(this);
    }
    return orElse();
  }
}

abstract class IntentAlreadyUsed implements IntentFailure {
  const factory IntentAlreadyUsed() = _$IntentAlreadyUsed;
}

/// @nodoc
abstract class $ExampleAlreadyUsedCopyWith<$Res> {
  factory $ExampleAlreadyUsedCopyWith(
          ExampleAlreadyUsed value, $Res Function(ExampleAlreadyUsed) then) =
      _$ExampleAlreadyUsedCopyWithImpl<$Res>;
}

/// @nodoc
class _$ExampleAlreadyUsedCopyWithImpl<$Res>
    extends _$IntentFailureCopyWithImpl<$Res>
    implements $ExampleAlreadyUsedCopyWith<$Res> {
  _$ExampleAlreadyUsedCopyWithImpl(
      ExampleAlreadyUsed _value, $Res Function(ExampleAlreadyUsed) _then)
      : super(_value, (v) => _then(v as ExampleAlreadyUsed));

  @override
  ExampleAlreadyUsed get _value => super._value as ExampleAlreadyUsed;
}

/// @nodoc

class _$ExampleAlreadyUsed implements ExampleAlreadyUsed {
  const _$ExampleAlreadyUsed();

  @override
  String toString() {
    return 'IntentFailure.exampleAlreadyUsed()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is ExampleAlreadyUsed);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() serverError,
    required TResult Function() intentAlreadyUsed,
    required TResult Function() exampleAlreadyUsed,
    required TResult Function() invalidIntentOrExample,
  }) {
    return exampleAlreadyUsed();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? serverError,
    TResult Function()? intentAlreadyUsed,
    TResult Function()? exampleAlreadyUsed,
    TResult Function()? invalidIntentOrExample,
  }) {
    return exampleAlreadyUsed?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? serverError,
    TResult Function()? intentAlreadyUsed,
    TResult Function()? exampleAlreadyUsed,
    TResult Function()? invalidIntentOrExample,
    required TResult orElse(),
  }) {
    if (exampleAlreadyUsed != null) {
      return exampleAlreadyUsed();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ServerError value) serverError,
    required TResult Function(IntentAlreadyUsed value) intentAlreadyUsed,
    required TResult Function(ExampleAlreadyUsed value) exampleAlreadyUsed,
    required TResult Function(InvalidIntentOrExample value)
        invalidIntentOrExample,
  }) {
    return exampleAlreadyUsed(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(ServerError value)? serverError,
    TResult Function(IntentAlreadyUsed value)? intentAlreadyUsed,
    TResult Function(ExampleAlreadyUsed value)? exampleAlreadyUsed,
    TResult Function(InvalidIntentOrExample value)? invalidIntentOrExample,
  }) {
    return exampleAlreadyUsed?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ServerError value)? serverError,
    TResult Function(IntentAlreadyUsed value)? intentAlreadyUsed,
    TResult Function(ExampleAlreadyUsed value)? exampleAlreadyUsed,
    TResult Function(InvalidIntentOrExample value)? invalidIntentOrExample,
    required TResult orElse(),
  }) {
    if (exampleAlreadyUsed != null) {
      return exampleAlreadyUsed(this);
    }
    return orElse();
  }
}

abstract class ExampleAlreadyUsed implements IntentFailure {
  const factory ExampleAlreadyUsed() = _$ExampleAlreadyUsed;
}

/// @nodoc
abstract class $InvalidIntentOrExampleCopyWith<$Res> {
  factory $InvalidIntentOrExampleCopyWith(InvalidIntentOrExample value,
          $Res Function(InvalidIntentOrExample) then) =
      _$InvalidIntentOrExampleCopyWithImpl<$Res>;
}

/// @nodoc
class _$InvalidIntentOrExampleCopyWithImpl<$Res>
    extends _$IntentFailureCopyWithImpl<$Res>
    implements $InvalidIntentOrExampleCopyWith<$Res> {
  _$InvalidIntentOrExampleCopyWithImpl(InvalidIntentOrExample _value,
      $Res Function(InvalidIntentOrExample) _then)
      : super(_value, (v) => _then(v as InvalidIntentOrExample));

  @override
  InvalidIntentOrExample get _value => super._value as InvalidIntentOrExample;
}

/// @nodoc

class _$InvalidIntentOrExample implements InvalidIntentOrExample {
  const _$InvalidIntentOrExample();

  @override
  String toString() {
    return 'IntentFailure.invalidIntentOrExample()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is InvalidIntentOrExample);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() serverError,
    required TResult Function() intentAlreadyUsed,
    required TResult Function() exampleAlreadyUsed,
    required TResult Function() invalidIntentOrExample,
  }) {
    return invalidIntentOrExample();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? serverError,
    TResult Function()? intentAlreadyUsed,
    TResult Function()? exampleAlreadyUsed,
    TResult Function()? invalidIntentOrExample,
  }) {
    return invalidIntentOrExample?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? serverError,
    TResult Function()? intentAlreadyUsed,
    TResult Function()? exampleAlreadyUsed,
    TResult Function()? invalidIntentOrExample,
    required TResult orElse(),
  }) {
    if (invalidIntentOrExample != null) {
      return invalidIntentOrExample();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ServerError value) serverError,
    required TResult Function(IntentAlreadyUsed value) intentAlreadyUsed,
    required TResult Function(ExampleAlreadyUsed value) exampleAlreadyUsed,
    required TResult Function(InvalidIntentOrExample value)
        invalidIntentOrExample,
  }) {
    return invalidIntentOrExample(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(ServerError value)? serverError,
    TResult Function(IntentAlreadyUsed value)? intentAlreadyUsed,
    TResult Function(ExampleAlreadyUsed value)? exampleAlreadyUsed,
    TResult Function(InvalidIntentOrExample value)? invalidIntentOrExample,
  }) {
    return invalidIntentOrExample?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ServerError value)? serverError,
    TResult Function(IntentAlreadyUsed value)? intentAlreadyUsed,
    TResult Function(ExampleAlreadyUsed value)? exampleAlreadyUsed,
    TResult Function(InvalidIntentOrExample value)? invalidIntentOrExample,
    required TResult orElse(),
  }) {
    if (invalidIntentOrExample != null) {
      return invalidIntentOrExample(this);
    }
    return orElse();
  }
}

abstract class InvalidIntentOrExample implements IntentFailure {
  const factory InvalidIntentOrExample() = _$InvalidIntentOrExample;
}
