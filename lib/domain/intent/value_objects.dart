import 'package:dartz/dartz.dart';
import 'package:telegram_bot/domain/core/failures.dart';
import 'package:telegram_bot/domain/core/value_objects.dart';
import 'package:telegram_bot/domain/core/value_validators.dart';

class IntentBody extends ValueObject<String> {
  @override
  final Either<ValueFailure<String>, String> value;

  factory IntentBody(String input) {
    return IntentBody._(intentValidate(input));
  }

  const IntentBody._(this.value);
}

class Example extends ValueObject<String> {
  @override
  final Either<ValueFailure<String>, String> value;

  factory Example(String input) {
    return Example._(right(input));
  }

  const Example._(this.value);
}
