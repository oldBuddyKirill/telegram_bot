import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:telegram_bot/presentation/core/app_widget.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  // Bloc.observer = Observer();
  runApp(AppWidget());
}

// class Observer extends BlocObserver {
//   @override
//   void onChange(BlocBase bloc, Change change) {
//     super.onChange(bloc, change);
//     print('${bloc.runtimeType} $change');
//   }
//
//   @override
//   void onEvent(bloc, event) {
//     super.onEvent(bloc, event);
//     print('Event: ${bloc.runtimeType} $event');
//   }
// }