import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:telegram_bot/domain/intent/intent.dart';
import 'package:telegram_bot/domain/intent/value_objects.dart';

part 'intent_dto.freezed.dart';
part 'intent_dto.g.dart';

@freezed
abstract class IntentDto implements _$IntentDto {
  const IntentDto._();

  factory IntentDto({
    required String intent,
    required List<String> examples,
  }) = _IntentDto;

  factory IntentDto.fromDomain(Intent intent) => IntentDto(
        intent: intent.intentBody.getOrCrash(),
        examples: intent.examples.map((example) => example.getOrCrash()).toList(),
        // examples: intent.examples.map((example) => ExampleDto.fromDomain(example)).toList(),
      );

  Intent toDomain() => Intent(
        intentBody: IntentBody(intent),
        examples: examples.map((example) => Example(example)).toList(),
      );

  factory IntentDto.fromJson(Map<String, dynamic> json) => _$IntentDtoFromJson(json);
}

