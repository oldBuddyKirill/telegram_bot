// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'intent_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_IntentDto _$$_IntentDtoFromJson(Map<String, dynamic> json) => _$_IntentDto(
      intent: json['intent'] as String,
      examples:
          (json['examples'] as List<dynamic>).map((e) => e as String).toList(),
    );

Map<String, dynamic> _$$_IntentDtoToJson(_$_IntentDto instance) =>
    <String, dynamic>{
      'intent': instance.intent,
      'examples': instance.examples,
    };
