import 'dart:convert';

import 'package:dartz/dartz.dart';
import 'package:flutter/foundation.dart';
import 'package:telegram_bot/domain/intent/i_intent_repository.dart';
import 'package:telegram_bot/domain/intent/intent.dart';
import 'package:telegram_bot/domain/intent/intent_failure.dart';
import 'package:telegram_bot/infrastructure/intent/intent_dto.dart';
import 'package:http/http.dart' as http;

class IntentRepository implements IIntentRepository{

  @override
  Future<Either<IntentFailure, List<Intent>>> loadIntents() async {
    try {
      final Uri uri = Uri.parse('https://telegram.api.syncleo-iot.com/config');
      http.Response response = await http.get(uri);
      if (kDebugMode) {
        print('Status code: ${response.statusCode}');
        print('Body: ${response.body}');
      }

      final data = response.body;
      final Iterable l = json.decode(data);
      final Iterable<Intent> dtos = l.map((item) => IntentDto.fromJson(item as Map<String, dynamic>).toDomain());
      final List<Intent> result = dtos.toList();
      return right(result);
    }
    catch (e) {
      return left(const IntentFailure.serverError());
    }
  }

  @override
  Future<Either<IntentFailure, Unit>> saveChanges(List<Intent> intents) async {
    try{
      final List<IntentDto> intentsDto = intents.map((intent) => IntentDto.fromDomain(intent)).toList();
      final Uri uri = Uri.parse('https://telegram.api.syncleo-iot.com/config');
      final String jsonBody = json.encode(intentsDto);
      final http.Response response = await http.post(uri, body: jsonBody);
      if (kDebugMode) {
        print('Status code: ${response.statusCode}');
        print('Response body: ${response.body}');
      }
      if (response.statusCode == 200) return right(unit);
      return left(const IntentFailure.serverError());
    } catch (e) {
      return left(const IntentFailure.serverError());
    }
  }

  @override
  Future<Either<IntentFailure, String>> getStatus() async {
    try{
      final Uri uri = Uri.parse('https://telegram.api.syncleo-iot.com/trainingStatus');
      http.Response response = await http.get(uri);
      final status = response.body;
      return right(status);
    } catch (e) {
      return left(const IntentFailure.serverError());
    }
  }
}