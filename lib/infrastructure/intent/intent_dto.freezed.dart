// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'intent_dto.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

IntentDto _$IntentDtoFromJson(Map<String, dynamic> json) {
  return _IntentDto.fromJson(json);
}

/// @nodoc
class _$IntentDtoTearOff {
  const _$IntentDtoTearOff();

  _IntentDto call({required String intent, required List<String> examples}) {
    return _IntentDto(
      intent: intent,
      examples: examples,
    );
  }

  IntentDto fromJson(Map<String, Object?> json) {
    return IntentDto.fromJson(json);
  }
}

/// @nodoc
const $IntentDto = _$IntentDtoTearOff();

/// @nodoc
mixin _$IntentDto {
  String get intent => throw _privateConstructorUsedError;
  List<String> get examples => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $IntentDtoCopyWith<IntentDto> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $IntentDtoCopyWith<$Res> {
  factory $IntentDtoCopyWith(IntentDto value, $Res Function(IntentDto) then) =
      _$IntentDtoCopyWithImpl<$Res>;
  $Res call({String intent, List<String> examples});
}

/// @nodoc
class _$IntentDtoCopyWithImpl<$Res> implements $IntentDtoCopyWith<$Res> {
  _$IntentDtoCopyWithImpl(this._value, this._then);

  final IntentDto _value;
  // ignore: unused_field
  final $Res Function(IntentDto) _then;

  @override
  $Res call({
    Object? intent = freezed,
    Object? examples = freezed,
  }) {
    return _then(_value.copyWith(
      intent: intent == freezed
          ? _value.intent
          : intent // ignore: cast_nullable_to_non_nullable
              as String,
      examples: examples == freezed
          ? _value.examples
          : examples // ignore: cast_nullable_to_non_nullable
              as List<String>,
    ));
  }
}

/// @nodoc
abstract class _$IntentDtoCopyWith<$Res> implements $IntentDtoCopyWith<$Res> {
  factory _$IntentDtoCopyWith(
          _IntentDto value, $Res Function(_IntentDto) then) =
      __$IntentDtoCopyWithImpl<$Res>;
  @override
  $Res call({String intent, List<String> examples});
}

/// @nodoc
class __$IntentDtoCopyWithImpl<$Res> extends _$IntentDtoCopyWithImpl<$Res>
    implements _$IntentDtoCopyWith<$Res> {
  __$IntentDtoCopyWithImpl(_IntentDto _value, $Res Function(_IntentDto) _then)
      : super(_value, (v) => _then(v as _IntentDto));

  @override
  _IntentDto get _value => super._value as _IntentDto;

  @override
  $Res call({
    Object? intent = freezed,
    Object? examples = freezed,
  }) {
    return _then(_IntentDto(
      intent: intent == freezed
          ? _value.intent
          : intent // ignore: cast_nullable_to_non_nullable
              as String,
      examples: examples == freezed
          ? _value.examples
          : examples // ignore: cast_nullable_to_non_nullable
              as List<String>,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_IntentDto extends _IntentDto {
  _$_IntentDto({required this.intent, required this.examples}) : super._();

  factory _$_IntentDto.fromJson(Map<String, dynamic> json) =>
      _$$_IntentDtoFromJson(json);

  @override
  final String intent;
  @override
  final List<String> examples;

  @override
  String toString() {
    return 'IntentDto(intent: $intent, examples: $examples)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _IntentDto &&
            const DeepCollectionEquality().equals(other.intent, intent) &&
            const DeepCollectionEquality().equals(other.examples, examples));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(intent),
      const DeepCollectionEquality().hash(examples));

  @JsonKey(ignore: true)
  @override
  _$IntentDtoCopyWith<_IntentDto> get copyWith =>
      __$IntentDtoCopyWithImpl<_IntentDto>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_IntentDtoToJson(this);
  }
}

abstract class _IntentDto extends IntentDto {
  factory _IntentDto({required String intent, required List<String> examples}) =
      _$_IntentDto;
  _IntentDto._() : super._();

  factory _IntentDto.fromJson(Map<String, dynamic> json) =
      _$_IntentDto.fromJson;

  @override
  String get intent;
  @override
  List<String> get examples;
  @override
  @JsonKey(ignore: true)
  _$IntentDtoCopyWith<_IntentDto> get copyWith =>
      throw _privateConstructorUsedError;
}
